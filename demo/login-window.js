import { defineComponent, ref, resolveDirective } from "../vue.esm.browser-3.0.js";
import { useRouter, useRoute } from "../vue-router.esm.browser-4.0.js";
import UltraMixin from "../ultra-mixin.js";
import UltraEventBus from "../ultra-event-bus.js";
import { FetchPost } from "../ultra-utils.js";

export default defineComponent({
	name: "login",
	components: { UltraEventBus },
	setup() {
		const loading = ref(false);
		const email = ref("");
		const password = ref("");
		const errorMessage = ref("");
		const U1Modal = resolveDirective("u1-modal");
		const $router = useRouter();
		const $route = useRoute();

		const save = function () {}
		const close = function () {}
		const login = function () {
			//Process login...
			loading.value = true;
			const formData = {
				email: email.value,
				password: password.value,
			};

			FetchPost("./api/login.json", formData)
				.then((json) => {
					loading.value = false;
					if (json.error) {
						UltraMixin.error(json.error);
					} else {
						if ($route.query["login-redirect"]) {
							$router.replace($route.query["login-redirect"]);
							// $router.replace({ name: "home" });
						} else {
							$router.replace({ name: "home" });
						}
					}
				})
				.catch((error) => {
					loading.value = false;
					console.error(error);
					UltraMixin.error(error);
				});
		}

		return {
			loading,
			email,
			password,
			errorMessage,
			U1Modal,
			save,
			close,
			login
		}
	},
	template: `<div v-u1-modal class="u1-window-modal" style="--u1-window-modal-bg: linear-gradient(#464c54,#292e35);">
  <section class="u1-window" style="--u1-window-width: 500px;">
    <div class="u1-window-header">
      <div class="u1-window-title">
				<div class="u1-cols-2-max-1fr u1-align-center u1-mobile-cols" data-cy-test="login-title">
					<i class="u1-i-- logo-image"></i>
	          ACCO Office Login
				</div>
			</div>
    </div>
    <div class="u1-window-body">
      <section class="u1 u1-relative u1-form">
        <div>
          <p class="u1-message-info" v-if="$route.query.redirect">
            You have been logged out. Please login again.
          </p>
          <form @submit.prevent="login" class="u1-rows" autocomplete="off">
            <div class="u1-label-input-rows">
              <label>Email</label>
              <input v-model="email" type="email" data-cy-test="email">
              <label>Password</label>
              <input v-model="password" type="password" data-cy-test="password">
            </div>
            <div class="u1-buttons-large" style="margin-bottom: 2rem;">
              <button type="submit" class="u1-button u1-button-gray u1-button-with-loading" data-cy-test="submit-button">
                <span>Login</span>
                <i v-show="loading" class="u1-loading-icon u1-invert u1-opacity-50" style="margin-left: 1rem;"></i>
              </button>
            </div>
          </form>
        </div>
      </section>
    </div>
  </section>
</div>
`,
});
