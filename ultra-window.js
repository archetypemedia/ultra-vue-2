import {defineComponent, ref} from './vue.esm.browser-3.0.js';
import UltraEventBus from "./event-bus.js";

export default defineComponent({
	name: "ultra-window",
	components: { UltraEventBus },
	props: {
		close: {
			default: function() {
				console.error("No close function declared.");
			},
			type: Function,
		},
	},
	setup() {
		const title = ref("Window Title");
		return {
			title
		}
	},
	template: `
  <div class="u1-window-modal">
    <section class="u1-window">
      <div class="u1-window-header">
        <div class="u1-window-title">
					<slot name="title">
						<div v-html="title"></div>
					</slot>
				</div>
        <button
          class="u1-button-window-close u1-invert u1-opacity-low-hover-100"
          @click="close()"
          type="button"
				></button>
				<slot name="menu">
					<menu class="u1-toolbar u1-window-toolbar">
						<li>
							<button @click="save()" class="u1-button u1-button-green" type="button">
								<i class="u1-i-save u1-opacity-50"></i>
								Save
							</button>
							<button @click="close()" class="u1-button" type="button">
								Cancel
							</button>
						</li>
					</menu>
				</slot>
      </div>
      <div class="u1-window-body">
				<slot>(Window content)</slot>
      </div>
    </section>
  </div>
`,
});
