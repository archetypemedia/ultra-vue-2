import { defineComponent, defineEmit } from "./vue.esm.browser-3.0.js";
/**
 * How to use:
 * <ultra-radio v-model="item.progress_status"
					:buttons="[{value:'In Progress'},{value:'Next'},{value:'Queue'}]"></ultra-radio>
 * OR
 * <ultra-radio v-model="item.progress_status"
					:buttons="[{value:1,label:'In Progress'},{value:2,label:'Next'},{value:3,label:'Queue'}]"></ultra-radio>
 */
export default defineComponent({
	name: "ultra-radio",
	props: {
		modelValue: String,
		buttons: Array,
		htmlLabels: false,
		classSelected: { type: String, default: "u1-radio-selected" },
		default: {type: [Object,Boolean], default: false }
	},
	setup(props) {
		const defaultButton = props.default; 
		// const emit = defineEmit(["update:modelValue"]);
		function updateValue(radioValue) {
			this.$emit('update:modelValue', radioValue) // previously was `this.$emit('input', title)`
		}

		function isString(theVar) {
			return typeof theVar == 'string';
		}

		return {
			isString,
			updateValue,
			props,
		};
	},
	template: `<section class="u1-radio-group">
	<label v-if="props.default" :class="modelValue == props.default.value ? classSelected:''">
		<input 
		type="radio" 
		:value="props.default.value" 
		:checked="modelValue == props.default.value" 
		@input="updateValue($event.target.value)" />

		<div v-if="props.htmlLabels" v-html="props.default.label ? props.default.label:props.default.value"
		class="u1-cols-2-1fr-max"
		></div>
		<template v-else>{{ props.default.label ? props.default.label:props.default.value }}</template>
	</label>
	<template v-for="(radioButton,index) in buttons">
		<label :class="modelValue == radioButton.value ? classSelected:''">
			<input 
			type="radio" 
			:value="radioButton.value" 
			:checked="modelValue == radioButton.value" 
			@input="updateValue($event.target.value)" />
			<div v-if="props.htmlLabels" v-html="radioButton.label ? radioButton.label:radioButton.value"
			:class="radioButton.class"
			></div>
			<template v-else>{{ radioButton.label ? radioButton.label:radioButton.value }}</template>
		</label>
	</template>
</section>
`,
});
