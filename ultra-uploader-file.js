import { defineComponent, reactive, ref, onMounted, onBeforeUnmount } from './vue.esm.browser-3.0.js';
import UltraEventBus from "./ultra-event-bus.js";
import ultraMixin from './ultra-mixin.js';

export default defineComponent({
	name: "ultra-uploader-file",
	components: { UltraEventBus },
	props: {
		file: {},
		uploadUrl: "",
		uploadDir: "",
		accept: "",
		postData: Array, // Format like [{name:'module',value:'categorized_items_7'},{name:'json_api',value:'upload-user-file'}]
		fileFieldName: "", //Used in template only as a debug detector to ensure this value is passed properly to the server
	},
	setup(props) {
		let xhr = {};
		const progress = ref(0);
		const image_src = ref("");
		const is_uploading = ref(false);
		const is_error = ref(false);
		const is_completed = ref(false);
		const upload_error = ref("");
		const validImageTypes = [
			"image/gif",
			"image/jpeg",
			"image/png",
			"image/webp",
			"image/svg",
		];

		const errorMessage = ref("");

		let { file, uploadUrl, uploadDir, accept, fileFieldName } = props;

		const isValidFileType = function (file, accept) {
			if (accept === '*') return true;

			const file_ext = file.name.split(".").pop();
			const file_mime = file.type;
			let acceptsRaw = accept.split(",");
			const accepts = acceptsRaw.map(str => str.trim());
			return accepts.some(mimeOrExt => {
				return "." + file_ext == mimeOrExt || file_mime.match(mimeOrExt);
			});
		}

		const abort = function () {
			xhr.abort();
			UltraEventBus.$emit("abort-file", file);
		}

		onBeforeUnmount(() => {
			abort();
		});

		onMounted(() => {
			is_uploading.value = true;

			var reader = new FileReader();
			reader.onload = e => {
				if (!isValidFileType(file, accept)) {
					UltraEventBus.$emit(
						"ultra-snackbar-error-persist",
						"This is not a valid file. Acceptable types are: " + accept
					);
					abort();
				}

				if (validImageTypes.includes(file.type)) {
					//Uploaded file is an image...
					image_src.value = reader.result;
				}
			};
			reader.readAsDataURL(file);

			//Upload each image...
			let formData = new FormData();
			formData.append(fileFieldName, file);
			formData.append('upload_dir', uploadDir);

			if (props.postData) {
				//Add arbitrary post data from parent to this post
				props.postData.forEach(postItem => {
					formData.append(postItem.name, postItem.value);
				});
			}

			xhr = new XMLHttpRequest();

			xhr.onloadstart = () => {
				is_uploading.value = true;
			};

			xhr.upload.onprogress = evt => {
				// let temp_progress = progress.value;
				let new_progress = Math.ceil((evt.loaded / evt.total) * 100);
				let progress_step = () => {
					//Animate progress...
					let step = (new_progress - progress.value) / 5;
					//Ensure the progress steps go smoothly and fast if upload is fast...
					step = step < 0.2 ? 0.2 : step;
					progress.value += step;
					if (progress.value < new_progress) {
						window.requestAnimationFrame(progress_step);
					}
				};
				window.requestAnimationFrame(progress_step);
			};

			xhr.onerror = evt => {
				is_error.value = true;
				is_uploading.value = false;
				is_completed.value = true;
				UltraEventBus.$emit("uploadend", file);
				UltraEventBus.$emit("ultra-snackbar-error", evt.target.statusText);
			};

			xhr.onabort = () => {
				is_uploading.value = false;
				is_completed.value = true;
				UltraEventBus.$emit("uploadend", file);
			};

			xhr.onload = evt => {
				is_uploading.value = false;

				// var jsonResponse = this.xhr.response;
				var jsonResponse = JSON.parse(evt.target.responseText);
				if (jsonResponse.error) {
					is_error.value = true;
					errorMessage.value = jsonResponse.error;
					UltraEventBus.$emit("ultra-snackbar-error", jsonResponse.error);
					if (jsonResponse.debug) ultraMixin.debug(jsonResponse.debug);
				} else {
					is_completed.value = true;
					progress.value = 100;
					UltraEventBus.$emit("upload-success", file, jsonResponse);
				}
			};

			// Add any event handlers here...
			xhr.open("POST", uploadUrl, true);
			xhr.send(formData);
		});

		return {
			progress,
			image_src,
			is_uploading,
			is_error,
			is_completed,
			upload_error,
			file,
			accept,
			fileFieldName,
			errorMessage,
			abort
		}
	},
	template: `
<transition name="u1-fade">
	<div :data-fileFieldName="fileFieldName" :class="(is_error ? 'u1-upload-file-preview-error':'') +' u1-upload-file-preview'" :style="'background-image:url('+image_src+');'">
		<div>{{file.name}}</div>
		<button class="u1-button u1-button-gray u1-opacity-med-hover-100" @click="abort" type="button">
			<i class="u1-i-close"></i>
		</button>
		<progress max="100" :value="progress"></progress>
		<div></div>
		<div v-if="is_error" class="u1-error">{{errorMessage}}</div>
  </div>
</transition>
`,
});
