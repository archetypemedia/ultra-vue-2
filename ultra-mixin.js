import UltraEventBus from "./ultra-event-bus.js";

export default {
	error: function(msg) {
		UltraEventBus.$emit("ultra-snackbar-error", msg);
	},
	errorPersist: function(msg) {
		UltraEventBus.$emit("ultra-snackbar-error-persist", msg);
	},
	debug: function(msg) {
		UltraEventBus.$emit("ultra-snackbar-debug", msg);
	},
	snackbar: function(msg) {
		UltraEventBus.$emit("ultra-snackbar", msg);
	}, 
	snackbarPersist: function(msg) {
		UltraEventBus.$emit("ultra-snackbar-persist", msg, {persist:true});
	}
};
