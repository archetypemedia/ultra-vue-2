import { defineComponent, reactive, ref, resolveDirective } from "../vue.esm.browser-3.0.js";
import { useRouter, useRoute } from "../vue-router.esm.browser-4.0.js";
import UltraMixin from "../ultra-mixin.js";
import UltraEventBus from "../ultra-event-bus.js";
import { MemberFetchPost } from "../ultra-utils.js";
import UltraTable from "../ultra-table.js";

export default defineComponent({
	name: "sds-doc",
	components: { UltraEventBus, UltraTable },
	setup() {
		const loading = ref(true);
		const doc = reactive({});
		const sdsUlid = ref("");
		const rawMaterialShow = ref("");
		const $router = useRouter();
		const $route = useRoute();
		const U1Window = resolveDirective('u1-window');

		const close = function () {
			$router.push({ name: "sds" });
		}

		const open1 = function () {
			$router.push({ name: "sds-doc-edit-1"});
		}

		const open2 = function () {
			$router.push({ name: "sds-doc-edit-2" });
		}

		const load = function () {
			loading.value = true;

			MemberFetchPost({}, "./api-members/sds-doc.json", { sds_ulid: $route.params.sdsUlid })
				.then((json) => {
					loading.value = false;
					if (json.error) {
						UltraMixin.error(json.error);
					} else {
						Object.keys(json).forEach(key => {
							doc[key] = json[key];
						});
					}
				})
				.catch((error) => {
					loading.value = false;
					UltraMixin.error(error);
				});
		}

		load();

		return {
			loading,
			doc,
			sdsUlid,
			rawMaterialShow,
			U1Window,
			close,
			open1,
			open2,
			load
		}
	},
	template: `<div class="u1-window-modal">
	<section v-u1-window style="--u1-window-width: 1200px" class="u1-window">
		<div class="u1-window-header">
			<div class="u1-window-title">
				<span class="u1-opacity-50">SDS <i class="u1-i-chevron"></i></span> <b>{{doc.product_name}}</b>
				<i v-if="loading" class="u1-loading-icon u1-invert"></i>
			</div>
			<i class="u1-i-close u1-opacity-low-hover-100 u1-pointer" @click="close()"></i>
			<menu class="u1-toolbar u1-window-toolbar">
				<li>
					<button @click="close()" class="u1-button" type="button"> Close </button>
					<button class="u1-button u1-button-blue" @click="pdf()" type="button">
						<i class="u1-i-arrow u1-i-rotate-down u1-opacity-50"></i>
						PDF
					</button>
				</li>
				<li class="u1-justify-end">
					<button class="u1-button" @click="" type="button">
						<i class="u1-i-trash u1-opacity-50 u1-color-red"></i>
						Trash
					</button>
				</li>
			</menu>
		</div>
		<div class="u1-window-body u1-window-mobile-compact">
			<section class="u1-rows u1-relative u1-form">
				<fieldset>
					<legend class="u1-sticky-top">
						<div class="u1-cols-2-1fr-max u1-align-center u1-mobile-cols">
							<div> 1. Identification of the substance/preparation and of the company/undertaking </div>
							<div>
								<button class="u1-button" @click="open1()" type="button">
									<i class="u1-i-pencil u1-color-orange"></i>
									Edit
								</button>
							</div>
						</div>
					</legend>
					<section class="u1-cols-2-max-1fr u1-label-input-rows" style="--u1-gap-row: 0.4rem">
						<label>Issue Date</label>
						<div> {{doc.issue_datetime}} </div>
						<label>Revision Date</label>
						<div> {{doc.revision_datetime}} </div>
						<label>Version</label>
						<div> {{doc.version}} </div>
						<label>Product Name</label>
						<div> {{doc.product_name}} </div>
						<label>UN/ID Number</label>
						<div> {{doc.un_id_number}} </div>
						<label>Synonyms</label>
						<div> {{doc.synonyms}} </div>
						<label>Recommended Use</label>
						<div> {{doc.recommended_use}} </div>
						<label>Uses Advised Against</label>
						<div> {{doc.uses_advised_against}} </div>
						<label>Manufacturer Address</label>
						<div v-if="doc.manufacturer_address">{{doc.manufacturer_address}}</div>
						<div v-else>(ACCO Address)</div>
						<label>Emergency Phone</label>
						<div> {{doc.emergency_phone}} </div>
					</section>
				</fieldset>

				<fieldset>
					<legend class="u1-sticky-top">
						<div class="u1-cols-2-1fr-max u1-align-center u1-mobile-cols">
							<div> 2a. Hazards Identification - Classifications </div>
							<div>
							
							</div>
						</div>
					</legend>

					<transition-group name="u1-fade">
						<div
							v-for="(item, index) in doc.hazard_classifications"
							:key="item.hazard_class_ulid"
							class="u1-cols-2-1fr-max u1-align-center u1-mobile-cols"
							style="padding: 1rem; margin: 1rem 0; background: rgba(0, 0, 0, 0.1); border-radius: 0.4rem"
						>
							<div class="u1-align-center u1-cols-2-max-1fr u1-mobile-cols">
								<img style="height: 32px" :src="'./images/hazards/'+item.pictogram+'.svg'" />
								<div> {{item.hazard_type}} {{item.hazard_category}} - {{item.signal_word}} - {{item.hazard_statement}} </div>
							</div>
							<button class="u1-button u1-button-red" @click="deleteHazard(item.hazard_class_ulid)" type="button">
								<i class="u1-i-trash"></i>
							</button>
						</div>
					</transition-group>
				</fieldset>
				<fieldset>
					<legend class="u1-sticky-top">
						<div class="u1-cols-2-1fr-max u1-align-center u1-mobile-cols">
							<div> 2b. Hazards Identification </div>
							<div>
								<button class="u1-button" @click="open2()" type="button">
									<i class="u1-i-pencil u1-color-orange"></i>
									Edit
								</button>
							</div>
						</div>
					</legend>

					<section class="u1-rows u1-label-input-rows u1-row">
						<label>OSHA Regulatory Status</label>
						<div class="pre-line"> {{doc.hazards_ident_osha_reg_status}} </div>
						<label>
								Hazards Signal Word
						</label>
						<div> {{doc.hazards_signal_word}} </div>
						<label>
							Hazards Statements
						</label>
						<div class="pre-line"> {{doc.hazards_statements}} </div>
						<label>
							Precautionary Statements - Prevention
						</label>
						<div class="pre-line"> {{doc.hazards_precaution_statements_prevention}} </div>
						<label>
							Precautionary Statements - Response
						</label>
						<div class="pre-line"> {{doc.hazards_precaution_statements_response}} </div>
						<label>
							Precautionary Statements - Storage
						</label>
						<div class="pre-line"> {{doc.hazards_precaution_statements_storage}} </div>
						<label>
							Precautionary Statements - Disposal
						</label>
						<div class="pre-line"> {{doc.hazards_precaution_statements_disposal}} </div>
						<label>
							Hazards not otherwise classified (HNOC) - Other Information
						</label>
						<div class="pre-line"> {{doc.hazards_precaution_statements_other}} </div>
					</section>
				</fieldset>

				<fieldset>
					<legend class="u1-sticky-top"> 3. Composition/information on ingredients </legend>
					<section class="u1-rows" style="--u1-gap-row: 0.4rem">
						<div>
							<button class="u1-button u1-button-green" @click="" type="button">
								<i class="u1-i-plus u1-opacity-50"></i>
								Add Raw Material
							</button></div
						>
						<div>
							<transition-group name="u1-fade">
								<div
									v-for="(item, index) in doc.raw_materials"
									:key="item.raw_material_ulid"
									class="u1-cols-2-max-1fr u1-align-start u1-mobile-cols u1-pointer"
									style="padding: 1rem; margin: 1rem 0; background: rgba(0, 0, 0, 0.1); border-radius: 0.4rem"
									@click="rawMaterialShow == item.raw_material_ulid ? rawMaterialShow = '':rawMaterialShow = item.raw_material_ulid"
								>
									<i
										:class="rawMaterialShow == item.raw_material_ulid ? 'u1-i-chevron u1-i-rotate-down u1-i-animate-rotate':'u1-i-chevron u1-i-animate-rotate'"
									></i>
									<div>
										<section class="u1-cols-2-1fr-max u1-align-center">
											<div class="u1-align-center u1-cols-2-1fr-max" style="grid-template-columns: 1fr max-content max-content">
												<div>{{item.chemical_name}}</div>
												<div v-if="item.cas_number">{{item.cas_number}}</div>
												<div v-else style="color: red"><em>(Missing CAS)</em></div>
												<button class="u1-button u1-button-orange" @click.stop="" type="button">
													<span v-if="item.weight_percent">{{item.weight_percent}}%</span>
													<span v-else style="color: red"><em>(Missing Weight %)</em></span>
													<i class="u1-i-pencil"></i>
												</button>
											</div>
											<div>
												<button class="u1-button" @click.stop="" type="button">
													<i class="u1-i-pencil u1-color-orange"></i>
													Edit SDS Properties
												</button>
												<button class="u1-button u1-button-red" @click.stop="" type="button">
													<i class="u1-i-trash"></i>
												</button>
											</div>
										</section>
										<section v-show="rawMaterialShow == item.raw_material_ulid">
											<div class="u1-cols-2-max-1fr u1-label-input-rows">
												<label>ACGIH TLV</label>
												<div>{{item.sds_exposure_acgih_tlv}}</div>
												<label>OSHA PEL</label>
												<div>{{item.sds_exposure_osha_pel}}</div>
												<label>NIOSH IDLH</label>
												<div>{{item.sds_exposure_niosh_idlh}}</div>
												<label>Oral LD50</label>
												<div>{{item.sds_toxicity_oral_ld50}}</div>
												<label>Dermal LD50</label>
												<div>{{item.sds_toxicity_dermal_ld50}}</div>
												<label>Inhalation LC50</label>
												<div>{{item.sds_toxicity_inhalation_lc50}}</div>
												<label>ACGIH</label>
												<div>{{item.sds_toxicity_carcinogencity_acgih}}</div>
												<label>IARC</label>
												<div>{{item.sds_toxicity_carcinogencity_iarc}}</div>
												<label>NTP</label>
												<div>{{item.sds_toxicity_carcinogencity_ntp}}</div>
												<label>OSHA</label>
												<div>{{item.sds_toxicity_carcinogencity_osha}}</div>
												<label>Algae/aquatic plants</label>
												<div>{{item.sds_ecological_aquatic_plants}}</div>
												<label>Fish</label>
												<div>{{item.sds_ecological_fish}}</div>
												<label>Crustacea</label>
												<div>{{item.sds_ecological_crustacea}}</div>

												<label>Partition coefficient</label>
												<div>{{item.sds_ecological_partition_coefficient}}</div>

												<label>California Hazardous Waste Status</label>
												<div>{{item.sds_disposal_california_waste_status}}</div>
												<label>Reportable Quantities</label>
												<div>{{item.sds_cwa_reportable_quantities}}</div>
												<label>Toxic Pollutants</label>
												<div>{{item.sds_cwa_toxic_pollutants}}</div>
												<label>Priority Pollutants</label>
												<div>{{item.sds_cwa_priority_pollutants}}</div>
												<label>Hazardous Substances</label>
												<div>{{item.sds_cwa_hazardous_substances}}</div>
												<label>Hazardous Substances RQs</label>
												<div>{{item.sds_cercla_hazardous_substances_rqs}}</div>
												<label>CERCLA/SARA RQ</label>
												<div>{{item.sds_cercla_sara_rqs}}</div>
												<label>Reportable Quantity (RQ)</label>
												<div>{{item.sds_cercla_reportable_quantities}}</div>
											</div>
										</section>
									</div>
								</div>
							</transition-group>
						</div>
					</section>
				</fieldset>
			</section>
			<div v-if="loading" class="u1-loading"></div>
		</div>
	</section>
	<router-view v-slot="{ Component }">
		<transition name="u1-fade">
			<component :is="Component"/>
		</transition>
	</router-view>
</div>
`,
});
