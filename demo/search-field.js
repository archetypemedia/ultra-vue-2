import { defineComponent, ref } from "../vue.esm.browser-3.0.js";
import UltraEventBus from "../ultra-event-bus.js";

export default defineComponent({
	name: "search-field",
	components: { UltraEventBus },
	props: {
		modelValue: {
			type: String,
		},
		insert: {
			type: String,
			default: "No information available",
		},
		label: { default: "No Info", type: String },
	},
	emits: ["update:modelValue"],
	setup(props, { emit }) {
		const search = ref("");
		const input = function () {
			emit("update:modelValue", search.value);
		}
		const reset = function () {
			search.value = '';
			emit("update:modelValue", search.value);
		}
		const keyup = function (event) {
			if (event.key === 'Escape') {
				search.value = '';
				emit('update:modelValue', search.value);
			}
		}

		return {
			search,
			input,
			reset,
			keyup
		}
	},
	template: `
		<section class="u1-form u1-input-with-icon u1-input-with-icon-after">
			<i class="u1-i-zoom u1-opacity-50"></i>
			<input type="search" @input="input" @keyup="keyup" class="u1-input-has-icon" placeholder="Search..." 
			style="width: 100%;box-shadow:none;line-height:1;font-weight:normal;" v-model="search" />
			<i v-show="search != ''" class="u1-i-close u1-opacity-low-hover-100 u1-pointer" @click="reset()" ></i>
		</section>
`,
});
