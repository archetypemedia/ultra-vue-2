import { defineComponent, onMounted } from "../vue.esm.browser-3.0.js";

export default defineComponent({
	name: "not-found",
  setup() {
    onMounted(() => {});
  },
	template: `<section class="component-main u1-relative">
  <!-- <header class="component-header">Not found error</header> -->
  <div class="component-body">
    <h2>Not found error</h2>
    <p>
      Sorry, the content you are looking for does not exist.
    </p>
  </div>
</section>
`,
});
