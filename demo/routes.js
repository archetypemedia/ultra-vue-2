import { createRouter, createWebHashHistory } from "../vue-router.esm.browser-4.0.js";
import UltraEventBus from "../ultra-event-bus.js";
import Index from "./index.js";
import LoginWindow from "./login-window.js";
import NotFound from "./not-found.js";

import DemoTable from "./example-table.js";
import exampleAutoTable from "./example-auto-table.js";
import ExampleMisc from "./example-misc.js";
import ExampleWindow from "./example-window.js";
import ExampleWindowSubWindow from "./example-window-sub-window.js";

import SdsDoc from "./sds-doc.js";
import SdsDocEdit1 from "./sds-doc-edit-1.js";
import SdsDocEdit2 from "./sds-doc-edit-2.js";

import DbEditor from "./db-editor.js";
import FormBuilder from "./form-builder.js";

const routes = [
	{ path: "/", component: Index, name: "home" },
	{ path: "/db-editor", component: DbEditor, name: "db-editor" },
	{ path: "/form-builder", component: FormBuilder, name: "form-builder" },
	{
		path: "/misc",
		component: ExampleMisc,
		name: "example-misc",
		children: [
			{
				path: "example-window/",
				name: "example-window",
				component: ExampleWindow,
				props: true,
				children: [
					{
						path: "sub-window/",
						name: "sub-window",
						component: ExampleWindowSubWindow,
						props: true,
					},
				],
			},
		],
	},
	{
		path: "/table",
		component: DemoTable,
		name: "sds",
		children: [
			{
				path: "doc/:sdsUlid",
				name: "sds-doc",
				component: SdsDoc,
				props: true,
				children: [
					{ path: "edit-1/", name: "sds-doc-edit-1", component: SdsDocEdit1, props: true },
					{ path: "edit-2/", name: "sds-doc-edit-2", component: SdsDocEdit2, props: true },
				],
			},
		],
	},
	{
		path: "/auto-table",
		component: exampleAutoTable,
		name: "auto-table",
		children: [
			{
				path: "doc/:sdsUlid",
				name: "sds-doc",
				component: SdsDoc,
				props: true,
				children: [
					{ path: "edit-1/", name: "sds-doc-edit-1", component: SdsDocEdit1, props: true },
					{ path: "edit-2/", name: "sds-doc-edit-2", component: SdsDocEdit2, props: true },
				],
			},
		],
	},
	{ path: "/login", component: LoginWindow, name: "login" },
	{ path: "/logout", component: LoginWindow, name: "logout" },
	{ path: "/:pathMatch(.*)*", component: NotFound },
];

const router = createRouter({
	history: createWebHashHistory(),
	routes, // short for `routes: routes`
});

router.beforeEach((to, from, next) => {
	if (to.name !== "login")
		() => {
			console.log("Redirect to login screen if not logged in here.");
		}; //Check auth
	next();
});

router.afterEach((to, from) => {
	UltraEventBus.$emit("nav-close");
});

export default router;
