export const U1Modal = {
	// Block scrolling on document body when modal open.
	beforeMount: function (el) {
		el.setAttribute("data-u1-modal","");
		document.documentElement.classList.add("u1-overflow-hidden");
	},
	unmounted: function (el) {
		el.removeAttribute("data-u1-modal");
		const els = document.querySelectorAll("[data-u1-modal]");
		//Only remove the overflow hidden if there are no more modals open.
		if(els.length <= 0)	document.documentElement.classList.remove("u1-overflow-hidden");
	},
};

export const U1Window = {
	// Add extra features to this window that is useful for Ultra 1 Css library
	mounted: function (el) {
		el.classList.add("u1-window-open");
	},
	updated: function (el) {
		//This must be in "updated", otherwise any :style="???" code will
		//interfere with setProperty() below.
		const headerEl = el.querySelector(".u1-window-header");
		if (headerEl) {
			el.style.setProperty("--u1-sticky-top", headerEl.offsetHeight + "px");
		}
	},
	unmounted: function (el) {
		el.classList.remove("u1-window-open");
		el.classList.add("u1-window-close");
	},
};

export const U1StickyTop = {
	// Add sticky position to this element.
	mounted: function (el) {
		el.style.setProperty("--u1-sticky-top", el.offsetTop + "px");
	},
};

export const U1Focus = {
	mounted: function (el) {
		// Focus the element
		el.focus();
	},
};

// Declare this externally so it can be bound/unbound easily.
const keypressEnter = function (evt, binding) {
	if (evt.key == "Enter") binding.value(); //Call function attached to this directive
};

export const U1InputsEnterKeypress = {
	// Automatically make ENTER keypress run a function
	mounted: function (el, binding) {
		const inputs = el.querySelectorAll("input");
		inputs.forEach((inputEl) => {
			inputEl.addEventListener("keypress", (evt) => {
				keypressEnter(evt, binding);
			});
		});
	},
	unmounted: function (el) {
		const inputs = el.querySelectorAll("input");
		inputs.forEach((inputEl) => {
			inputEl.removeEventListener("keypress", keypressEnter);
		});
	},
};

// Declare this externally so it can be bound/unbound easily.
const expandable = function (el) {
	//Auto-expanding textarea
	const hSet = getComputedStyle(el).getPropertyValue('--u1-expandable-height');
	const hCurrent = `${el.scrollHeight + 2}px`;
	if (hSet == hCurrent) return; //Height is the same, so don't trigger redraw...

	el.classList.add("u1-expandable");
	el.style.cssText = "--u1-expandable-height: " + (el.scrollHeight + 2) + "px;";
};

export const U1Expandable = {
	mounted: function (el) {
		if (!el) return;
		["keydown", "input", "mousedown", "click", "change"].forEach((eventName) =>
			el.addEventListener(
				eventName,
				() => {
					expandable(el);
				},
				false
			)
		);
		setTimeout(() => {
			expandable(el); //Run right now?
		}, 500);
	},
	unmounted: function (el) {
		["keydown", "input", "mousedown", "click"].forEach((evt) =>
			el.removeEventListener(
				evt,
				() => {
					expandable(el);
				},
				false
			)
		);
	},
};
