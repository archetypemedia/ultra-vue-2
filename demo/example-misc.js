import { defineComponent, reactive, ref, resolveDirective } from "../vue.esm.browser-3.0.js";
import { useRouter } from "../vue-router.esm.browser-4.0.js";
import UltraMixin from "../ultra-mixin.js";
import UltraEventBus from "../ultra-event-bus.js";
import { MemberFetchPost, FetchPost } from "../ultra-utils.js";
import UltraTable from "../ultra-table.js";
import UltraUploader from "../ultra-uploader.js";

export default defineComponent({
	name: "misc",
	components: { UltraTable, UltraUploader },
	setup() {
		const loading = ref(false);
		const search = ref("");
		const rawMaterials = reactive({value: []});
		const memberFetchPostResults = ref("");
		const loadingMemberFetchPost = ref(false);
		const fetchPostResults = ref("")
		const loadingFetchPost = ref(false);
		const loadingFetchPostError = ref(false);
		const loadingMemberFetchPostError = ref(false);
		const U1Focus = resolveDirective('u1-focus');
		const $router = useRouter();

		const confirmationOpen = function() {
			const msg = "Are you sure?<p></p>Warning note.";
			UltraEventBus.$emit("u1-confirm", msg, () => {
				//ACTION-TO-COMMIT
				alert("Do this thing!");
			});
		}

		const windowOpen = function() {
			$router.push({ name: "example-window" });
		}

		const memberFetchPost = function() {
			memberFetchPostResults.value = "";
			loadingMemberFetchPost.value = true;
			const authObject = {}; //This is simulated, and in the real system would manage member authentication
			const formData = {};
			MemberFetchPost(authObject, "./api-members/member-fetch-post.json", formData)
				.then(json => {
					loadingMemberFetchPost.value = false;
					if (json.error) {
						UltraMixin.error(json.error);
					} else {
						memberFetchPostResults.value = json.message;
					}
				})
				.catch(error => {
					loadingMemberFetchPost.value = false;
					UltraMixin.error(error);
				});
		}

		const fetchPost = function () {
			fetchPostResults.value = "";
			loadingFetchPost.value = true;
			const formData = {};
			FetchPost("./api/fetch-post.json", formData)
				.then(json => {
					loadingFetchPost.value = false;
					if (json.error) {
						UltraMixin.error(json.error);
					} else {
						fetchPostResults.value = json.message;
					}
				})
				.catch(error => {
					loadingFetchPost.value = false;
					UltraMixin.error(error);
				});
		}

		const fetchPostErrorUrl = function () {
			fetchPostResults.value = "";
			loadingFetchPost.value = true;
			const formData = {};
			FetchPost("./api/url-does-not-exist.json", formData)
				.then(json => {
					loadingFetchPost.value = false;
					//Url does not exist, so it will cause error.
				})
				.catch(error => {
					loadingFetchPost.value = false;
					UltraMixin.error(error);
				});
		}

		const fetchPostErrorServer = function () {
			fetchPostResults.value = "";
			loadingFetchPost.value = true;
			const formData = {};
			FetchPost("./api/error.json", formData)
				.then(json => {
					loadingFetchPost.value = false;
					if (json.error) {
						UltraMixin.error(json.error);
					} else {
						//The json results have an error message, so this won't get triggered.
					}
				})
				.catch(error => {
					loadingFetchPost.value = false;
					UltraMixin.error(error);
				});
		}

		const snackbarError = function(msg) {
			UltraEventBus.$emit("ultra-snackbar-error", "Snackbar error display");
		}

		const snackbarErrorPersist = function() {
			UltraEventBus.$emit("ultra-snackbar-error-persist", "Snackbar error with persistence.");
		}

		const snackbarShow = function() {
			UltraEventBus.$emit("ultra-snackbar", "Show a standard snackbar.");
		}

		const snackbarPersist = function() {
			UltraEventBus.$emit("ultra-snackbar", "Snackbar with persistence.", { persist: true });
		}

		return {
			loading,
			search,
			rawMaterials: rawMaterials.value,
			memberFetchPostResults,
			loadingMemberFetchPost,
			fetchPostResults,
			loadingFetchPost,
			loadingFetchPostError,
			loadingMemberFetchPostError,
			U1Focus,
			confirmationOpen,
			windowOpen,
			memberFetchPost,
			fetchPost,
			fetchPostErrorUrl,
			fetchPostErrorServer,
			snackbarError,
			snackbarErrorPersist,
			snackbarShow,
			snackbarPersist
		}
	},
	template: `<section class="component-main u1-relative">
	<header class="component-header">
		<div class="component-header-title" style="flex-grow: 0"> Misc Components </div>
	</header>
	<div class="component-body" style="--u1-sticky-top: var(--header-height)">
		<section class="u1-cols-2 u1-form" style="margin-top:2rem;">
			<section class="">
				<div v-if="loading" class="u1-loading"></div>
				<fieldset>
					<legend>Confirmation Box</legend>
					<button class="u1-button u1-button-gray" @click="confirmationOpen()" type="button">
						<i class="u1-i-check u1-opacity-50"></i>
						Open Confirmation Box
					</button>
				</fieldset>
				<fieldset>
					<legend>Windows</legend>
					<p>This uses the router to open a component. The component target is the "router-view" element below.</p>
					<button class="u1-button u1-button-blue" @click="windowOpen()" type="button">
						<i class="u1-i-check u1-opacity-50"></i>
						Open Window
					</button>
					<p><b>NOTE:</b> There is a "Sub Window" inside this window.</p>
				</fieldset>
				<fieldset>
					<legend>Snackbar</legend>
					<p>These snackbars will timeout and fade away.</p>
					<button class="u1-button u1-button-gray" @click="snackbarShow()" type="button">
						<i class="u1-i-check u1-opacity-50"></i>
						Show Snackbar
					</button>
					<button class="u1-button u1-button-red" @click="snackbarError()" type="button">
						<i class="u1-i-check u1-opacity-50"></i>
						Show Snackbar - Error
					</button>
				</fieldset>
				<fieldset>
					<legend>Snackbar with Persist option</legend>
					<p>These snackbars will exist until closed.</p>
					<button class="u1-button u1-button-gray" @click="snackbarPersist()" type="button">
						<i class="u1-i-check u1-opacity-50"></i>
						Show Snackbar - Persist
					</button>
					<button class="u1-button u1-button-red" @click="snackbarErrorPersist()" type="button">
						<i class="u1-i-check u1-opacity-50"></i>
						Show Snackbar - Error Persist
					</button>
				</fieldset>
			</section>
			<section>
				<fieldset>
					<p>Upload an image. (default settings)</p>
					<legend>Uploader</legend>
					<ultra-uploader></ultra-uploader>
				</fieldset>
				<fieldset>
					<legend>Fetch Post</legend>
					<p>This form of fetch call is assumed to be publicly accessible.</p>
					<button type="button" @click="fetchPost()" class="u1-button u1-button-green u1-button-with-loading">
						<span>Load Fetch Post</span>
						<i v-show="loadingFetchPost" class="u1-loading-icon u1-invert u1-opacity-50" style="margin-left: 1rem"></i>
					</button>
					<div class="u1-message" v-show="fetchPostResults">{{ fetchPostResults }}</div>
				</fieldset>
				<fieldset>
					<legend>Member Fetch Post</legend>
					<p>This form of fetch call sends an extra authorization object with the request to test for valid login with the request.</p>
					<button type="button" @click="memberFetchPost()" class="u1-button u1-button-green u1-button-with-loading">
						<span>Load Member Fetch Post</span>
						<i v-show="loadingMemberFetchPost" class="u1-loading-icon u1-invert u1-opacity-50" style="margin-left: 1rem"></i>
					</button>
					<div class="u1-message" v-show="memberFetchPostResults">{{ memberFetchPostResults }}</div>
				</fieldset>
				<fieldset>
					<legend>Fetch-Post Errors</legend>
					<section class="u1-grid">
						<button type="button" @click="fetchPostErrorUrl()" class="u1-button u1-button-red"> URL does not exist </button>
						<button type="button" @click="fetchPostErrorServer()" class="u1-button u1-button-red"> Server returns error message </button>
					</section>
				</fieldset>
			</section>
		</section>

		<router-view v-slot="{ Component }">
			<transition name="u1-fadein">
				<component :is="Component" />
			</transition>
		</router-view>
		<router-view name="slideright" v-slot="{ Component }">
		<transition name="u1-slideright">
			<component :is="Component" />
		</transition>
		</router-view>
	</div>
</section>
`,
});
