import { defineComponent, onMounted, reactive } from "./vue.esm.browser-3.0.js";
import UltraEventBus from "./ultra-event-bus.js";

export default defineComponent({
	name: "ultra-snackbar",
	setup() {
		const snackbars = reactive([]);

		const snackbarHide = function (index) {
			if (snackbars[index]) snackbars[index].show = false;

			if (
				!snackbars.some(snackbar => {
					return snackbar.show;
				})
			) {
				//Only clear out the snackbars after all are hidden, this
				//keeps managing snackbars very simple using only index keys
				//based on count/position in array.
				snackbars.splice(0, snackbars.length);
			}
		}

		const add = function (message, options = { is_error: false, persist: false, debug: false }) {
			let snackbar = {
				message: message,
				is_error: options.is_error,
				show: true,
				debug: options.debug,
			};

			let count = snackbars.push(snackbar);
			if (!options.persist) {
				let index = count - 1;
				setTimeout(() => {
					snackbarHide(index);
				}, 5000);
			}
		}

		const error = function (message, options = {}) {
			options.is_error = true;
			add(message, options);
		}

		const errorPersist = function (message, options = {}) {
			options.persist = true;
			error(message, options);
		}

		const debug = function (message, options = {}) {
			options.persist = true;
			options.debug = true;
			error(message, options);
		}

		onMounted(() => {
			UltraEventBus.$on("ultra-snackbar-error", error);
			UltraEventBus.$on("ultra-snackbar-error-persist", errorPersist);
			UltraEventBus.$on("ultra-snackbar-message", add);
			UltraEventBus.$on("ultra-snackbar-debug", debug);
			UltraEventBus.$on("ultra-snackbar", add);
		});

		const classes = (snackbar) => {
			let classes = ['u1-snackbar'];
			if (snackbar.is_error) classes.push('u1-snackbar-error');
			if (snackbar.debug) classes.push('u1-snackbar-debug');
			return classes.join(' ');
		}

		return {
			snackbars,
			add,
			error,
			snackbarHide,
			classes,
		}
	},
	template: `
	<transition-group class="u1-snackbars" name="u1-fade" tag="div">
		<div v-show="snackbar.show" :class="classes(snackbar)" 
		v-for="(snackbar,index) in snackbars" v-bind:key="index+'snackbar'">
			<div v-if="snackbar.debug" v-html="snackbar.message"></div>
			<div v-else>
				{{snackbar.message}}
			</div>
			<i @click="snackbarHide(index)" class="u1-i-close u1-pointer u1-hover-box u1-snackbar-close"></i>
		</div>
	</transition-group>`,
});
