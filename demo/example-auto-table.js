import {defineComponent, resolveDirective, ref, reactive, watch, computed } from "../vue.esm.browser-3.0.js";
import { useRouter, useRoute } from "../vue-router.esm.browser-4.0.js";
import UltraMixin from "../ultra-mixin.js";
import UltraEventBus from "../ultra-event-bus.js";
import { MemberFetchPost } from "../ultra-utils.js";
import ultraAutoTable from "../ultra-auto-table.js";
import SearchField from "./search-field.js";

export default defineComponent({
	name: "example-auto-table",
	components: { UltraEventBus, ultraAutoTable, SearchField },
	setup() {
		const loading = ref(false);
		const search = ref("");
		const docs = reactive({value: []});
		const U1Focus = resolveDirective('u1-focus');
		const $router = useRouter();
		const $route = useRoute();

		watch(
			() => $route,
			(to, from) => {
				if (to.name == "sds") {
					load();
				}
			}
		);

		const openSds = function (sdsUlid) {
			this.$router.push({ name: "sds-doc", params: { sdsUlid: sdsUlid } });
		}

		const load = function () {
			loading.value = true;
			MemberFetchPost({}, "./api-members/sds-all.json")
				.then((json) => {
					loading.value = false;
					if (json.error) {
						UltraMixin.error(json.error);
					} else {
						docs.value = json;
					}
				})
				.catch((error) => {
					loading.value = false;
					UltraMixin.error(error);
				});
		}

		const addDoc = function () {
			$router.push({ name: "sds-doc-new" });
		}

		// moved from created hook
		load();
		UltraEventBus.$on("load-sds-all", () => {
			load();
		});

		return {
			loading,
			search,
			docs,
			U1Focus,
			openSds,
			load,
			addDoc
		}
	},
	template: `<section class="component-main u1-relative">
	<header class="component-header">
		<div class="component-header-title" style="flex-grow: 0"> Auto-Table </div>
		<div style="flex-grow: 2; flex-shrink: 1">
		</div>
	</header>
	<div class="component-body" style="--u1-sticky-top: var(--header-height)">
	 <div v-if="loading" class="u1-loading"></div>
		<ultra-auto-table  :rows="docs" class="u1-table u1-table-sortable u1-row" style="--u1-mobile-cols: 2"></ultra-auto-table>
		<router-view v-slot="{ Component }">
			<transition name="u1-fadein">
				<component :is="Component" />
			</transition>
		</router-view>
		<router-view name="slideright" v-slot="{ Component }">
			<transition name="u1-slideright">
				<component :is="Component" />
			</transition>
		</router-view>
	</div>
</section>
`,
});
