export const PathUpOneLevel = (path) => {
	//Return a path that has the last directory trimmed off.
	let pathUpOne = path.endsWith('/') ? path.slice(0, -1) : path;
	pathUpOne = pathUpOne.substring(0, pathUpOne.lastIndexOf('/'));
	return pathUpOne;
};

export const Debounce = function (callback, wait = 500) {
	console.log("DEBOUNCE u1", callback);
	let timeout;
	return (...args) => {
		const context = this;
		clearTimeout(timeout);
		timeout = setTimeout(() => callback.apply(context, args), wait);
	};
};

export const FetchPost = async function post(url = "", formDataOrObject = {}) {
	let formData = new FormData();
	if (!(formDataOrObject instanceof FormData)) {
		//Allow sending data as simple object.
		Object.entries(formDataOrObject).forEach((item) => {
			formData.append(item[0], item[1]);
		});
	} else {
		formData = formDataOrObject;
	}

	const response = await fetch(url, {
		method: "POST",
		body: formData,
	});
	return await response.json();
};

export const MemberFetchPost = async function post(
	auth,
	url = "",
	formDataOrObject = {}
) {
	let formData = new FormData();
	if (!(formDataOrObject instanceof FormData)) {
		//Allow sending data as simple object.
		Object.entries(formDataOrObject).forEach((item) => {
			formData.append(item[0], item[1]);
		});
	} else {
		formData = formDataOrObject;
	}
	const response = await fetch(url, {
		method: "POST",
		body: formData,
	});

	const monitor = response.clone();
	const json_monitor = monitor.json();

	json_monitor.then((json) => {
		if (json.loggedin == false) {
			if (auth.notLoggedIn) auth.notLoggedIn();
		}
	});

	return await response.json();
};
