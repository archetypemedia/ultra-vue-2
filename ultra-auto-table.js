import { defineComponent, reactive, ref, onMounted, computed, watchEffect, watch, isReactive } from "./vue.esm.browser-3.0.js";
import ultraEventBus from "./ultra-event-bus.js";

export default defineComponent({
	// USE:
	// WARNING: :rows REQUIRES a reactive() variable...
	// <ultra-auto-table
	// @tr-click="trClick($event)"
	// :rows="docs"
	// :thead="[{field:'progress_status',label:'Progresso!'},{field:'territory_manager_name',label:'TM!!'}]"
	// :colgroup="{progress_status:140,need_expedited:100,date_received:130,item_width:60,item_height:60}"
	// ></ultra-auto-table>

	name: "ultra-auto-table",
	props: {
		rows: { type: Object, required: false },
		loading: Boolean,
		autoThLabels: { type: Boolean, default: true },
		colLimit: { type: Number, default: 10 }, //Only applies to auto-built table columns.
		thead: { type: Object, default: { value: [] } },
		colgroup: { type: Object, default: {} },
		sortable: { type: Boolean, default: true },
		sortDefault: String,
		tableId: { type: String, default: "table" },
		trClick: { type: Function },
		hideSearch: false,
		searchClass: { type: String, default: 'u1-auto-table-search' },
		persistSort: { type: Boolean, default: true },
		persistSearch: { type: Boolean, default: true },
		messageNoData: { type: String, default: 'No rows to show...' },
		messageNoSearchResults: { type: String, default: 'Nothing found, try a different search.' },
	},
	setup(props, context) {
		//Error check props
		if (!isReactive(props.rows)) {
			console.error('Prop rows is REQUIRED to be a reactive()!');
			ultraEventBus.$emit('ultra-snackbar-error-persist', 'UltraAutoTable: Property "rows" is required to be reactive.');
		}

		if (props.thead.value.count > 0 && !isReactive(props.thead)) {
			console.error('Prop thead is REQUIRED to be a reactive()!');
			ultraEventBus.$emit('ultra-snackbar-error-persist', 'UltraAutoTable: Property "thead" is required to be reactive.');
		}

		const SORT_DIRECTION_KEY = 'ultra-auto-table-sort-directions-' + window.location;
		const SEARCH_KEY = 'ultra-auto-table-search-' + window.location;

		const sortClassDefault = "u1-i-chevrons-out u1-opacity-25 u1-i-rotate-up";
		const classes = {
			sortedUp: "u1-i-chevron u1-i-rotate-up",
			sortedDown: "u1-i-chevron u1-i-rotate-down",
			thSorted: "u1-th-is-sorted u1-color-blue",
		};
		const trRows = reactive({ value: [] });
		const trRowsSorted = reactive({ value: [] });
		const trRowsSearched = reactive({ value: [] });
		const sortClasses = reactive({ value: {} });
		const sortDirections = reactive({ value: {} });
		const thead = reactive({ value: props.thead.value });
		//TODO: change thead to use toRef() to convert to a proper reactive system
		const search = ref("");
		const searchClass = ref(props.searchClass);

		const thLabel = function (label) {
			if (props.thead.length <= 0 && props.autoThLabels) {
				return label.replace(/_/g, " ");
			}
			return label;
		};

		watch(props.rows, (rows, prevRows) => {
			//ROWS changed from PROPS, this an external change to table data
			//so this change overrides any internal data.
			//Clone this data to trRowsSorted for sorting/filtering purposes
			initTable(rows);

			//Check to see if there is a persistent search
			const storedSearch = getStoredSearch();
			if (props.persistSearch && storedSearch) {
				//NOTE: Search is REQUIRED to happen before sorting
				search.value = storedSearch;
				doSearch(storedSearch);
			}

			if (props.persistSort && getStoredSortDirections()) {
				//Check to see if the table should be sorted based on stored directions
				//Sort table right now...
				sortTable();
			}
		});

		const initTable = (rows) => {
			trRowsSorted.value = [...rows.value];
			trRowsSearched.value = [...rows.value];
			trRows.value = [...rows.value];
			//--THEAD
			//TODO: change thead to use toRef() to convert to a proper reactive system

			if (props.thead.value.length > 0) {
				thead.value = props.thead.value;
			} else {
				//Build thead automatically from first row of data
				rows.value.every(item => {
					Object.keys(item).forEach(key => {
						if (props.colLimit == false || thead.value.length < props.colLimit) {
							thead.value.push({ label: key, field: key });
						}
					});
				});
			}
		}
		// console.log('props.rows.value',props.rows.value);
		if (props?.rows?.value?.length > 0) {
			//There is data ready in props right now, so trigger display.
			initTable(props.rows);
		}	

		if (isReactive(props.thead)) {
			watch(props.thead, (newThead) => {
				thead.value = newThead.value;
			});
		}

		const tableClasses = computed(() => {
			let classes = ["u1-table"];
			if (props.sortable) classes.push("u1-table-sortable");
			return classes.join(" ");
		});

		const trClick = (row) => {
			context.emit("tr-click", row);
		};

		const sortByColumn = function (field, evt) {
			if (!field) {
				console.error('Missing attribute "data-sort" on <th> element to allow sorting.');
			}

			//If SHIFT is pressed, just ADD, otherwise RESET -- NOT DONE YET
			if (field) {
				if (!evt.shiftKey) {
					//Clear out all other fields
					Object.entries(sortDirections.value).forEach(([savedField, value]) => {
						if (savedField != field) delete sortDirections.value[savedField];
					});
				}

				//Toggle sort field values ASC,DESC,off
				if (sortDirections.value[field] == undefined) {
					// ASC
					sortDirections.value[field] = true;
				} else if (sortDirections.value[field] == true) {
					// DESC
					sortDirections.value[field] = false;
				} else {
					// Default Sort... 3rd click
					delete sortDirections.value[field];
				}
			}
			//Save the sort directions for persistent 
			storeSortDirection();
			sortTable();
		};

		const getSortClass = (field) => {
			//Determine sort class based on direction of sort for this field
			if (sortDirections.value[field] == true) {
				// ASC
				return classes.sortedUp;
			} else if (sortDirections.value[field] == false) {
				// DESC
				return classes.sortedDown;
			} else {
				//Default sort class
				return sortClassDefault;
			}
		}

		const getSortClassTh = (field) => {
			//Determine the TH sort class based on direction of sort for this field
			if (sortDirections.value[field] == true) {
				// ASC
				return classes.thSorted;
			} else if (sortDirections.value[field] == false) {
				// DESC
				return classes.thSorted;
			} else {
				//Default sort class
				return '';
			}
		}

		const storeSortDirection = () => {
			localStorage.setItem(SORT_DIRECTION_KEY, JSON.stringify(sortDirections.value));
		}

		const getStoredSortDirections = () => {
			let sortDir = localStorage.getItem(SORT_DIRECTION_KEY);
			sortDir = JSON.parse(sortDir);
			return sortDir;
		}

		const sortTable = function () {
			const storedSortDirections = getStoredSortDirections();

			if (storedSortDirections) {
				//Use the stored sort directions....
				sortDirections.value = storedSortDirections;
			}

			//Reset sort before each sort, so that when sort directions are added/removed
			//then the default sorting on each column will display correctly
			if (isFiltered()) {
				//Sort from the filtered values
				trRowsSorted.value = [...trRowsSearched.value];
			} else {
				//Sort from the default values in props
				trRowsSorted.value = [...props.rows.value];
			}

			var doSort, ASC, DESC;
			trRowsSorted.value.sort((a, b) => {
				// Sort the actual data...
				if (!Object.keys(sortDirections.value).length) {
					//No fields selected to sort by...
					if (props.sortDefault) {
						return a[props.sortDefault] < b[props.sortDefault];
					} else {
						return 0;
					}
				}

				for (const sortField in sortDirections.value) {
					if (sortDirections.value[sortField] == true) {
						ASC = 1;
						DESC = -1;
					} else if (sortDirections.value[sortField] == false) {
						ASC = -1;
						DESC = 1;
					}
					if (a[sortField] > b[sortField]) {
						doSort = ASC;
						break;
					}
					if (a[sortField] < b[sortField]) {
						doSort = DESC;
						break;
					}
				}
				if (doSort != 0) return doSort;
				return 0;
			});
			updateTable(trRowsSorted);
		};

		//Searching
		const reset = function () {
			search.value = "";
		};

		const keyup = function (event) {
			if (event.key === "Escape") {
				search.value = "";
			}
		};

		watch(search, (newSearch, prevSearch) => {
			if (props.persistSearch) {
				//Store the search for later use.
				storeSearch(search.value);
			}
			doSearch(newSearch);
		});

		const isFiltered = function () {
			return !!search.value;
		};

		const storeSearch = (search) => {
			localStorage.setItem(SEARCH_KEY, search);
		}

		const getStoredSearch = () => {
			let search = localStorage.getItem(SEARCH_KEY);
			return search;
		}

		const isSorted = function () {
			return Object.entries(sortDirections.value).length >= 0;
		};

		const updateTable = function (newTrRows) {
			trRows.value = [...newTrRows.value];
		};

		const doSearch = (searchVal) => {
			//Search through the sorted values.
			trRowsSearched.value = [...props.rows.value];

			if (!searchVal) {
				//Reset the search
				updateTable(props.rows);
				if (isSorted()) sortTable();
				return;
			}

			const searchLower = searchVal.toLowerCase();
			trRowsSearched.value = trRowsSearched.value.filter(row => {
				return Object.values(row).some(fieldValue => {
					if (fieldValue === null) return false;
					if (fieldValue == "") return false;
					return fieldValue
						.toString()
						.toLowerCase()
						.includes(searchLower);
				});
			});
			updateTable(trRowsSearched);
			if (isSorted()) sortTable();
		};

		return {
			searchClass,
			thead,
			trRows,
			tableClasses,
			sortClasses,
			sortClassDefault,
			search,
			getSortClassTh,
			getSortClass,
			sortByColumn,
			trClick,
			thLabel,
			reset,
			keyup,
		};
	},
	template: `<table :class="tableClasses" style="--u1-mobile-cols: 2">
	<colgroup>
		<col v-for="(th) in thead.value" :width="colgroup[th.field] ? colgroup[th.field]:'' " />
	</colgroup>
	<thead :style="autoThLabels ? 'text-transform: capitalize;':''">
		<tr v-if="!hideSearch" 
			><th :colspan="thead.value.length">
				<div :class="searchClass">
					<section
						style="width: clamp(30%, 320px, 100%)"
						:class="(search ? 'u1-color-blue':'') + ' u1-form u1-input-with-icon u1-input-with-icon-after'"
					>
						<i class="u1-i-zoom u1-opacity-50"></i>
						<input
							type="search"
							@keyup="keyup"
							class="u1-input-has-icon"
							placeholder="Search..."
							style="width: 100%; box-shadow: none; line-height: 1; font-weight: normal"
							v-model="search"
						/>
						<i v-show="search != ''" class="u1-i-close u1-opacity-med-hover-100 u1-pointer" @click="reset()"></i>
					</section>
				</div>
			</th>
		</tr>
		<tr>
			<th v-for="(th,index) in thead.value" @click="sortByColumn(th.field,$event)" :data-sort="th.field" :class="getSortClassTh(th.field)">
				<div class="u1-cols-2-1fr-max u1-mobile-cols" style="--u1-gap-col:0;"
					><span>{{thLabel(th.label)}}</span> <i :class="getSortClass(th.field)"></i
				></div>
			</th>
		</tr>
	</thead>
	<tbody is="transition-group" name="u1-fade">
		<tr v-if="trRows.value.length <= 0 && !loading">
			<td v-if="search" :colspan="thead.value.length">{{messageNoSearchResults}}</td>
			<td v-else :colspan="thead.value.length">{{messageNoData}}</td>
		</tr>
		<tr v-for="row in trRows.value" :key="row" @click="trClick(row)" class="u1-pointer">
			<td v-for="(td,index) in thead.value">
					<div v-if="td.isHtml" v-html="row[td.field]"></div>
					<button
						class="u1-button"
						type="button"
						v-else-if="td.hasFunc"
						@click.stop="td.func( row) "
						v-html="row[td.field]"
					></button>
					<template v-else>{{row[td.field]}}</template>
				</td>
		</tr>
	</tbody>
</table>
`,
});
