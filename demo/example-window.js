import { defineComponent, ref, reactive, resolveDirective } from "../vue.esm.browser-3.0.js";
import { useRouter } from "../vue-router.esm.browser-4.0.js";
import UltraEventBus from "../ultra-event-bus.js";

export default defineComponent({
	name: "example-window",
	components: { UltraEventBus },
	setup() {
		const loading = ref(false);
		const item = reactive({
			name: "",
			description: ""
		});
		const itemUlid = ref("");
		const fromSdsDoc = ref(false);
		const U1Window = resolveDirective("u1-window");
		const $router = useRouter();

		const close = function () {
			$router.push({name: 'example-misc'});
		}

		const openSubWindow = function () {
			$router.push({ name: "sub-window" });
		}

		return {
			loading,
			item,
			itemUlid,
			fromSdsDoc,
			U1Window,
			close,
			openSubWindow
		}
	},
	template: `<div class="u1-window-modal">
	<section v-u1-window class="u1-window" >
		<div class="u1-window-header">
			<div class="u1-window-title">
				Example Window
			</div>
			<i class="u1-i-close u1-opacity-low-hover-100 u1-pointer" @click="close()"></i>
			<menu class="u1-toolbar u1-window-toolbar">
				<li>
					<button class="u1-button u1-button-green" @click="close()" type="button">
						<i class="u1-i-save u1-opacity-50"></i>
						Save
					</button>
					<button @click="close()" class="u1-button" type="button"> Cancel </button>
				</li>
				<li class="u1-justify-end">
					<button class="u1-button" type="button">
						<i class="u1-i-trash u1-color-red"></i>
						Trash
					</button>
				</li>
			</menu>
		</div>
		<div class="u1-window-body u1-window-mobile-compact">
			<section class="u1-rows u1-relative u1-form">

					<div>
						<button class="u1-button u1-button-blue" @click="openSubWindow()" type="button">
							<i class="u1-i-arrow u1-opacity-50"></i>
							Open Sub-Window
						</button>
					</div>
			The sub-window will use the "router-view" element at the bottom of this component's template.
				<div>
					<label>Item Name</label>
					<input v-model="item.name" type="text" />
				</div>
				<div>
					<label>Item Description</label>
					<input v-model="item.description" type="text" />
				</div>
			</section>
		</div>
	</section>
	<router-view v-slot="{ Component }">
		<transition name="u1-fade">
			<component :is="Component" />
		</transition>
	</router-view>
</div>
`,
});
