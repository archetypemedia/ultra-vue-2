import { defineComponent, defineEmit, onMounted, ref, watch } from "./vue.esm.browser-3.0.js";
/**
 * How to use:
 * <ultra-checkbox v-model="item.progress_status"
					:button="[{value:'In Progress'},{value:'Next'},{value:'Queue'}]"></ultra-checkbox>
 * OR
 * <ultra-checkbox v-model="item.progress_status"
					:button="[{value:1,label:'In Progress'},{value:2,label:'Next'},{value:3,label:'Queue'}]"></ultra-checkbox>
 */
export default defineComponent({
	name: "ultra-checkbox-single",
	props: {
		modelValue: [Boolean,String],
		label: String,
		classSelected: { type: String, default: "u1-checkbox-selected" },
	},

	setup(props) {
		function updateValue(checkboxValue) {
			this.$emit('update:modelValue', checkboxValue);
		}

		function isChecked(value) {
			//Required to ensure truthy values work with this module
			return value === "1" || value === true || value === "true";
		}

		return {
			isChecked,
			updateValue,
		};
	},
	template: `<section class="u1-checkbox-group">
	<label :class="!!modelValue ? classSelected:''">
		<input 
			type="checkbox"
			:checked="isChecked(modelValue)"
			@input="updateValue($event.target.checked)" />
		{{ label }}
	</label>
</section>
`,
});
