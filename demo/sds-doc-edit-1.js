import { defineComponent, ref, reactive, onMounted, resolveDirective } from "../vue.esm.browser-3.0.js";
import { useRouter, useRoute } from "../vue-router.esm.browser-4.0.js";
import UltraMixin from "../ultra-mixin.js";
import UltraEventBus from "../ultra-event-bus.js";
import { FetchPost } from "../ultra-utils.js";

export default defineComponent({
	name: "sds-doc-edit-1",
	components: { UltraEventBus },
	setup() {
		const loading = ref(true);
		const doc = reactive({});
		const sdsUlid = ref("");
		const $router = useRouter();
		const $route = useRoute();
		const U1Modal = resolveDirective('u1-modal');
		const U1Window = resolveDirective('u1-window');
		const U1InputsEnterKeypress = resolveDirective('u1-inputs-enter-keypress');

		const save = function () {
			loading.value = true;
			const formData = {
				sds_ulid: sdsUlid.value,
				doc: JSON.stringify(doc),
			};

			FetchPost("./api-members/sds-doc-save-1.json", formData)
				.then((json) => {
					loading.value = false;
					if (json.error) {
						UltraMixin.error(json.error);
					} else {
						close();
					}
				})
				.catch((error) => {
					loading.value = false;
					UltraMixin.error(error);
				});
		}

		const close = function () {
			$router.replace({ name: "sds-doc" });
		}

		const load = function () {
			loading.value = true;
			const formData = {
				sds_ulid: sdsUlid.value,
			};
			FetchPost("./api-members/sds-doc.json", formData)
				.then((json) => {
					loading.value = false;
					if (json.error) {
						UltraMixin.error(json.error);
					} else {
						Object.keys(json).forEach(key => {
							doc[key] = json[key];
						});
					}
				})
				.catch((error) => {
					loading.value = false;
					UltraMixin.error(error);
				});
		}

		onMounted(() => {
			sdsUlid.value = $route.params.sdsUlid;
			load();
		});

		return {
			loading,
			doc,
			sdsUlid,
			U1Modal,
			U1Window,
			U1InputsEnterKeypress,
			save,
			close,
			load
		}
	},
	template: `<div class="u1-window-modal">
	<section v-u1-modal v-u1-window class="u1-window">
		<div class="u1-window-header">
			<div class="u1-window-title">
				<span class="u1-opacity-50">SDS Info <i class="u1-i-chevron"></i></span> {{doc.product_name}}
			</div>
			<i class="u1-i-close u1-opacity-low-hover-100 u1-pointer" @click="close()"></i>
			<menu class="u1-toolbar u1-window-toolbar">
				<li>
					<button @click="save()" class="u1-button u1-button-green" type="button">
						<i class="u1-i-save u1-opacity-50"></i>
						Save
					</button>
					<button @click="close()" class="u1-button" type="button"> Cancel </button>
				</li>
			</menu>
		</div>
		<div class="u1-window-body" v-u1-inputs-enter-keypress="save">
			<section class="u1-rows u1-relative u1-form">
				<section class="u1-cols-2">
					<div> 
						Issue Date:
						<input type="text" v-model="doc.issue_datetime" />
					</div>
					<div> 
					Revision Date:
					<input type="text" v-model="doc.revision_datetime" />
					</div>
				</section>
				<section class="u1-label-input-rows">
					<label>Version</label>
					<div>
						<input type="text" v-model="doc.version" />
					</div>
					<label>Product Name</label>
					<div>
						<input type="text" v-model="doc.product_name" />
					</div>
					<label>UN/ID Number</label>
					<div>
						<input type="text" v-model="doc.un_id_number" />
					</div>
					<label>Synonyms</label>
					<div>
						<input type="text" v-model="doc.synonyms" />
					</div>
					<label>Recommended Use</label>
					<div>
						<input type="text" v-model="doc.recommended_use" />
					</div>
					<div class="u1-cols-2-1fr-max u1-mobile-cols">
						<label>Uses Advised Against</label>
					</div>
					<div>
						<input type="text" v-model="doc.uses_advised_against" />
					</div>
					<div class="u1-cols-2-1fr-max u1-mobile-cols">
						<label>Manufacturer Address</label>
					</div>
					<div>
						<input v-show="doc.manufacturer_address != ''" type="text" v-model="doc.manufacturer_address" />
						<div v-show="doc.manufacturer_address == ''">
							Anderson Chemical Company
						</div>
					</div>
					<label>Emergency Phone</label>
					<div>
						<input type="text" v-model="doc.emergency_phone" />
					</div>
				</section>
				<div v-if="loading" class="u1-loading"></div>
			</section>
		</div>
	</section>
</div>
`,
});
