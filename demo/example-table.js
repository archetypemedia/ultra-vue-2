import {defineComponent, resolveDirective, ref, reactive, watch, computed } from "../vue.esm.browser-3.0.js";
import { useRouter, useRoute } from "../vue-router.esm.browser-4.0.js";
import UltraMixin from "../ultra-mixin.js";
import UltraEventBus from "../ultra-event-bus.js";
import { MemberFetchPost } from "../ultra-utils.js";
import UltraTable from "../ultra-table.js";
import SearchField from "./search-field.js";

export default defineComponent({
	name: "sds-main",
	components: { UltraEventBus, UltraTable, SearchField },
	setup() {
		const loading = ref(false);
		const search = ref("");
		const docs = reactive({value: []});
		const U1Focus = resolveDirective('u1-focus');
		const $router = useRouter();
		const $route = useRoute();

		const docsFiltered = computed(() => {
			if (!search.value) return docs.value;
			const searchLower = search.value.toLowerCase();
			const found = docs.value.filter((doc) => {
				return (
					doc.product_name.toLowerCase().includes(searchLower) ||
					(doc.cas_numbers && doc.cas_numbers.toLowerCase().includes(searchLower)) ||
					doc.un_id_number.toLowerCase().includes(searchLower)
				);
			});
			console.log(found);
			return found;
		});
		watch(
			() => $route,
			(to, from) => {
				if (to.name == "sds") {
					load();
				}
			}
		);

		const openSds = function (sdsUlid) {
			this.$router.push({ name: "sds-doc", params: { sdsUlid: sdsUlid } });
		}

		const load = function () {
			loading.value = true;
			MemberFetchPost({}, "./api-members/sds-all.json")
				.then((json) => {
					loading.value = false;
					if (json.error) {
						UltraMixin.error(json.error);
					} else {
						docs.value = json;
					}
				})
				.catch((error) => {
					loading.value = false;
					UltraMixin.error(error);
				});
		}

		const addDoc = function () {
			$router.push({ name: "sds-doc-new" });
		}

		// moved from created hook
		load();
		UltraEventBus.$on("load-sds-all", () => {
			load();
		});

		return {
			loading,
			search,
			docs,
			U1Focus,
			docsFiltered,
			openSds,
			load,
			addDoc
		}
	},
	template: `<section class="component-main u1-relative">
	<header class="component-header">
		<div class="component-header-title" style="flex-grow: 0"> SDS </div>
		<div style="flex-grow: 2; flex-shrink: 1">
			<search-field v-model="search"></search-field>
		</div>
	</header>
	<div class="component-body" style="--u1-sticky-top: var(--header-height)">
		<div v-if="loading" class="u1-loading"></div>
		<p v-else-if="docs.value.length <= 0" class="u1-message-note"> There are no SDS documents created yet. </p>
		<p v-else-if="docsFiltered.length <= 0" class="u1-message-note"> No documents found containing "{{search}}". </p>
		<ultra-table v-else :rows="docsFiltered" class="u1-table u1-table-sortable u1-row" style="--u1-mobile-cols: 2">
			<template #colgroup>
				<col />
				<col width="180" />
				<col width="20%" />
			</template>
			<template #thead="slotProps">
				<tr>
					<th data-sort="product_name">
						<div class="u1-cols-2-1fr-max u1-mobile-cols"> <span>Product Name</span> <i :class="slotProps.sortClass('product_name')"></i> </div>
					</th>
					<th data-sort="un_id_number">
						<div class="u1-cols-2-1fr-max u1-mobile-cols"> <span>UN/ID No.</span> <i :class="slotProps.sortClass('un_id_number')"></i> </div>
					</th>
					<th data-sort="cas_numbers">
						<div class="u1-cols-2-1fr-max u1-mobile-cols"> <span>CAS Numbers</span> <i :class="slotProps.sortClass('cas_numbers')"></i> </div>
					</th>
				</tr>
			</template>
			<template v-slot:default="slotProps" is="transition-group" name="u1-fade">
				<tr :key="row.sds_ulid" @click="openSds(row.sds_ulid)" v-for="row in slotProps.rows" class="u1-pointer">
					<td style="order: -1">
						<b>{{row.product_name}}</b>
					</td>
					<td>{{row.un_id_number}}</td>
					<td>{{row.cas_numbers}}</td>
				</tr>
			</template>
		</ultra-table>
		<router-view v-slot="{ Component }">
			<transition name="u1-fadein">
				<component :is="Component" />
			</transition>
		</router-view>
		<router-view name="slideright" v-slot="{ Component }">
			<transition name="u1-slideright">
				<component :is="Component" />
			</transition>
		</router-view>
	</div>
</section>
`,
});
