import { defineComponent, reactive, ref, onMounted, computed } from "./vue.esm.browser-3.0.js";
import UltraEventBus from "./ultra-event-bus.js";

export default defineComponent({
	// USE:
	// 	<ultra-table :rows="rows" class="u1-table u1-table-sortable">
	// 	<template #colgroup>
	// 		<col width="50">
	// 		<col>
	// 		<col>
	// 		<col width=150>
	// 		<col width=50 style="color:red;">
	// 		<col width=160>
	// 	</template>
	// 	<template #thead="slotProps">
	// 		<tr>
	// 			<th data-sort="title">
	// 				<div class="u1-cols-2-1fr-max"><span>Title</span> <i :class="slotProps.sortClass('title')"></i></div>
	// 			</th>
	// 			<th data-sort="stuff">
	// 				<div class="u1-cols-2-1fr-max"><span>Stuff</span> <i :class="slotProps.sortClass('stuff')"></i></div>
	// 			</th>
	// 		</tr>
	// 	</template>
	// 	<template v-slot:default="slotProps">
	// 		<tr v-for="row in slotProps.rows" class="u1-pointer">
	// 			<td>
	// 				{{row.title}}
	// 			</td>
	// 			<td>
	// 				{{row.stuff}}
	// 			</td>
	// 		</tr>
	// 		slotProps: {{slotProps}}
	// 	</template>
	// </ultra-table>

	name: "ultra-table",
	components: { UltraEventBus },
	props: {
		rows: Array,
		loading: Boolean,
		sortable: { type: Boolean, default: true },
		sortDefault: String,
		tableId: { type: String, default: "table" },
		classThIsSorted: { type: String, default: "u1-th-is-sorted u1-color-blue" },
	},
	setup(props) {
		let rowsClone = reactive({ value: [] });
		const sortDirections = reactive({ value: [] });

		const rowsComputed = computed(() => {
			if (Object.entries(sortDirections.value).length == 0) return props.rows;
			return rowsClone.value;
		});

		const tableClasses = computed(() => {
			let classes = ["u1-table"];
			if (props.sortable) classes.push("u1-table-sortable");
			return classes.join(" ");
		});

		const sort = function(evt) {
			let localSortDirections = window.localStorage.getItem(props.tableId + "-sort");
			localSortDirections = JSON.parse(localSortDirections);
			localSortDirections = localSortDirections || {};

			let field = evt.target.closest("th").getAttribute("data-sort");

			if (!field) {
				console.error('Missing attribute "data-sort" on <th> element to allow sorting.');
			}

			//If SHIFT is pressed, just ADD, otherwise RESET -- NOT DONE YET
			if (field) {
				if (!evt.shiftKey) {
					//Clear out all other fields
					Object.entries(sortDirections).forEach(([savedField, value]) => {
						if (savedField != field) delete sortDirections[savedField];
					});
				}
				//Toggle sort field values ASC,DESC,off
				if (localSortDirections[field] == undefined) {
					// ASC
					localSortDirections[field] = true;
				} else if (localSortDirections[field] == true) {
					// DESC
					localSortDirections[field] = false;
				} else if (!evt.shiftKey) {
					// Default Sort... 3rd click
					delete localSortDirections[field];
				} else {
					localSortDirections[field] = true;
				}
			}

			window.localStorage.setItem(props.tableId + "-sort", JSON.stringify(localSortDirections));
			sortTable();
		};

		const sortTable = function() {
			let localSortDirections = window.localStorage.getItem(props.tableId + "-sort");
			localSortDirections = JSON.parse(localSortDirections);
			localSortDirections = localSortDirections || {};
			sortDirections.value = localSortDirections;

			rowsClone.value.sort((a, b) => {
				// Sort the actual data...
				var doSort, ASC, DESC;
				if (!Object.keys(localSortDirections).length) {
					//No fields selected to sort by...
					if (props.sortDefault) {
						return a[props.sortDefault] < b[props.sortDefault];
					} else {
						return 0;
					}
				}

				for (const sortField in localSortDirections) {
					if (localSortDirections[sortField] == true) {
						ASC = 1;
						DESC = -1;
					} else if (localSortDirections[sortField] == false) {
						ASC = -1;
						DESC = 1;
					}

					if (a[sortField] > b[sortField]) {
						doSort = ASC;
						break;
					}
					if (a[sortField] < b[sortField]) {
						doSort = DESC;
						break;
					}
				}
				if (doSort != 0) return doSort;
				return 0;
			});
		};

		const sortClassTh = function(field) {
			let localSortDirections = window.localStorage.getItem(props.tableId + "-sort");
			localSortDirections = JSON.parse(localSortDirections);
			localSortDirections = localSortDirections || {};
			if (localSortDirections[field] !== undefined) return props.classThIsSorted;
		};

		const sortClass = function(field) {
			let localSortDirections = window.localStorage.getItem(props.tableId + "-sort");
			localSortDirections = JSON.parse(localSortDirections);
			localSortDirections = localSortDirections || {};

			if (localSortDirections[field] == true) return "u1-i-chevron u1-i-rotate-up";
			if (localSortDirections[field] == false) return "u1-i-chevron u1-i-rotate-down";
			return "u1-i-chevrons-out u1-opacity-25 u1-i-rotate-up";
		};

		onMounted(() => {
			rowsClone.value = JSON.parse(JSON.stringify(props.rows));
		});

		return {
			rowsComputed,
			tableClasses,
			sort,
			sortClass,
			sortClassTh,
		};
	},
	template: `<table :class="tableClasses">
		<colgroup><slot name="colgroup"></slot></colgroup>
		<thead @click="sort($event)"><slot :sortClassTh="sortClassTh" :sortClass="sortClass" name="thead"></slot></thead>
		<tbody>
			<slot :rows="rowsComputed"></slot>
		</tbody>
</table>`,
});
