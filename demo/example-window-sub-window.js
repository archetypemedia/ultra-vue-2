import { defineComponent, resolveDirective } from "../vue.esm.browser-3.0.js";
import UltraEventBus from "../ultra-event-bus.js";

export default defineComponent({
	name: "example-window-sub-window",
	components: { UltraEventBus },
	setup() {
		const close = function () {
			this.$router.push({ name: "example-window" });
		}

		const U1Window = resolveDirective('u1-window');

		return {
			close,
			U1Window
		}
	},
	template: `<div class="u1-window-modal">
	<section v-u1-window class="u1-window" style="max-width:400px;">
		<div class="u1-window-header">
			<div class="u1-window-title">
				<span class="u1-opacity-50">Example Window <i class="u1-i-chevron"></i></span>
				<b>Sub-Window</b>
			</div>
			<i class="u1-i-close u1-opacity-low-hover-100 u1-pointer" @click="close()"></i>
			<menu class="u1-toolbar u1-window-toolbar">
				<li>
					<button @click="close()" class="u1-button" type="button"> Close </button>
				</li>
				<li class="u1-justify-end">
	
				</li>
			</menu>
		</div>
		<div class="u1-window-body u1-window-mobile-compact">
			<section class="u1-form">
				<h2>This is a sub-window to the main example window</h2>
				<p>
Quo sit ut vero enim voluptatem est quia autem. Quis rem quo fuga nobis. Soluta et quibusdam eligendi sed sit est molestiae.
</p>
			</section>
		</div>
	</section>
</div>
`,
});
