import { defineComponent, ref, onUnmounted, onMounted } from './vue.esm.browser-3.0.js'
import { useRouter } from "./vue-router.esm.browser-4.0.js";
import UltraEventBus from "./ultra-event-bus.js";
import { U1Focus } from "./ultra-directives.js";
/*
How to use:
const msg = "Are you sure you want to clear this report?<p></p>There is no undo for this step.";
UltraEventBus.$emit("u1-confirm", msg, () => {
	// CODE TO RUN ON "ok"
});
*/
export default defineComponent({
	name: "ultra-confirm",
	directives: { U1Focus },
	setup() {
		const message = ref("");
		const show = ref(false);
		const width = ref("320px");
		const labelConfirm = ref("Ok");
		const labelDecline = ref("Cancel");
		const $router = useRouter();

		const confirmCallback = ref(function () {
			alert("Missing required confirmCallback for confirmation box.");
		})

		const unregisterRouterGuard =  ref(function () {})

		const open = (incomingMessage, callback, options = {}) => {
			//Check if Vue Router is being used, and if so, use a watcher to check for "back"
			if ($router) {
				unregisterRouterGuard.value = $router.beforeEach(
					(to, from, next) => {
						next(false); // abort the current navigation, we just want to close this modal.
						close();
					}
				);
			}

			labelConfirm.value = options.labelConfirm || labelConfirm.value;
			labelDecline.value = options.labelDecline || labelDecline.value;
			message.value = incomingMessage;
			show.value = true;
			confirmCallback.value = callback || confirmCallback.value;
		}

		const close = function () {
			unregisterRouterGuard.value();
			show.value = false;
		}

		const sendDecline = function () {
			close();
		}

		const sendConfirm = function () {
			confirmCallback.value();
			close();
		}

		onUnmounted(() => {
			unregisterRouterGuard.value();
		});

		onMounted(() => {
			UltraEventBus.$on("u1-confirm", (msg, confirmCallback, options) => {
				open(msg, confirmCallback, options);
			});
		})

		return {
			message,
			show,
			width,
			labelConfirm,
			labelDecline,
			confirmCallback,
			unregisterRouterGuard,
			open,
			close,
			sendDecline,
			sendConfirm
		}
	},
	template: `<transition name="u1-fade">
  <div v-if="show" class="u1-window-modal" tabindex="0" @keyup.esc="close()" v-u1-focus>
    <section class="u1-window" :style="'--u1-window-width:' + width">
      <div class="u1-window-body">
        <section class="u1-rows u1-relative u1-form">
          <div v-html="message"></div>
          <div>
            <button class="u1-button u1-button-green" @click="sendConfirm()" type="button">
              <i class="u1-i-check u1-opacity-50"></i>
              {{labelConfirm}}
            </button>
            <button class="u1-button" @click="sendDecline()" type="button">
              {{labelDecline}}
            </button>
          </div>
        </section>
      </div>
    </section>
  </div>
</transition>
`,
});
