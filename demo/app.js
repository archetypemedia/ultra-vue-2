import { createApp, onMounted, nextTick, reactive, ref } from "../vue.esm.browser-3.0.js";
import * as directives from "../ultra-directives.js";
import UltraEventBus from "../ultra-event-bus.js";
import UltraSnackbar from "../ultra-snackbar.js";
import UltraConfirm from "../ultra-confirm.js";
import Router from "./routes.js";

var app = createApp({
	components: { Router, UltraSnackbar, UltraEventBus, UltraConfirm },
	setup() {
		const items = reactive({value: []});
		const navShow = ref(false);
		const loading = ref(true);
		
		const closeNav = function () {
			navShow.value = false;
		}

		const toggleNav = function () {
			navShow.value = !navShow.value;
		}

		const navHome = function () {
			closeNav();
			this.$router.push({ name: "home" });
		}

		onMounted(() => {
			nextTick(() => {
				loading.value = false;
			});
			UltraEventBus.$on("nav-open", () => {
				navOpen();
			});
			UltraEventBus.$on("nav-close", () => {
				closeNav();
			});
			UltraEventBus.$on("nav-toggle", () => {
				toggleNav();
			});
		});
		
		return {
			items: items.value,
			navShow,
			loading,
			closeNav,
			toggleNav,
			navHome
		}
	}
});

const config = {
	//Add GLobal configs here: eg.
	// boss: 'Pointy Hair',
	// would accessible via this.$config.boss
	message: "my global message",
	isLoggedIn: true,
	install: () => {
		Object.defineProperty(app.config.globalProperties, "$config", {
			get() {
				return config;
			},
		});
	},
};

// adding directives
app.directive('u1-window', directives.U1Window);
app.directive('u1-focus', directives.U1Focus);
app.directive('u1-expandable', directives.U1Expandable);
app.directive('u1-inputs-enter-keypress', directives.U1InputsEnterKeypress);
app.directive('u1-modal', directives.U1Modal);
app.directive('u1-sticky-top', directives.U1StickyTop);

app.use(Router).use(config).mount('#app');


export default app;
