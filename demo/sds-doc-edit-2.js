import { defineComponent, onMounted, reactive, ref, resolveDirective } from "../vue.esm.browser-3.0.js";
import { useRouter } from "../vue-router.esm.browser-4.0.js";
import UltraMixin from "../ultra-mixin.js";
import UltraEventBus from "../ultra-event-bus.js";
import { FetchPost } from "../ultra-utils.js";

export default defineComponent({
	name: "sds-doc-edit-2",
	components: { UltraEventBus },
	setup() {
		const loading = ref(true);
		const doc = reactive({});
		const sdsUlid = ref("");
		const U1Modal = resolveDirective('u1-modal');
		const U1Window = resolveDirective('u1-window');
		const U1InputsEnterKeypress = resolveDirective('u1-inputs-enter-keypress');
		const U1Expandable = resolveDirective('u1-expandable');
		const $router = useRouter();

		const close = function () {
			$router.replace({ name: "sds-doc" });
		}
		
		const save = function () {
			loading.value = true;
			const formData = {
				sds_ulid: sdsUlid.value,
				doc: JSON.stringify(doc),
			};

			FetchPost("./api-members/?sds-doc-save-2", formData)
				.then((json) => {
					loading.value = false;
					if (json.error) {
						UltraMixin.error(json.error);
					} else {
						close();
					}
				})
				.catch((error) => {
					loading.value = false;
					UltraMixin.error(error);
				});
		}

		const load = function () {
			loading.value = true;
			const formData = {
				sds_ulid: sdsUlid.value,
			};
			FetchPost("./api-members/sds-doc.json", formData)
				.then((json) => {
					loading.value = false;
					if (json.error) {
						UltraMixin.error(json.error);
					} else {
						Object.keys(json).forEach(key => {
							doc[key] = json[key];
						});
					}
				})
				.catch((error) => {
					loading.value = false;
					UltraMixin.error(error);
				});
		}

		onMounted(() => {
			load();
		})

		return {
			loading,
			doc,
			sdsUlid,
			U1Modal,
			U1Window,
			U1InputsEnterKeypress,
			U1Expandable,
			close,
			save,
			load
		}
	},
	template: `<div class="u1-window-modal">
	<section v-u1-modal v-u1-window class="u1-window" style="--u1-window-width:1400px;">
		<div class="u1-window-header">
			<div class="u1-window-title">
				<span class="u1-opacity-50">2. Hazards Identification <i class="u1-i-chevron"></i></span> {{doc.product_name}}
			</div>
			<i class="u1-i-close u1-opacity-low-hover-100 u1-pointer" @click="close()"></i>
			<menu class="u1-toolbar u1-window-toolbar">
				<li>
					<button @click="save()" class="u1-button u1-button-green" type="button">
						<i class="u1-i-save u1-opacity-50"></i>
						Save
					</button>
					<button @click="close()" class="u1-button" type="button"> Cancel </button>
				</li>
			</menu>
		</div>
		<div class="u1-window-body" v-u1-inputs-enter-keypress="save">
			<section class="u1-label-input-rows u1-relative u1-form" style="margin-bottom: 10vh">
				<label>OSHA Regulatory Status</label>
				<div><textarea wrap="off" style="min-height: 300px" v-model="doc.hazards_ident_osha_reg_status"></textarea></div>
				<label>Hazard Signal Word</label>
				<div><input type="text" v-model="doc.hazards_signal_word"/></div>
				<label>Hazard Statements</label>
				<div><textarea wrap="off" style="min-height: 300px" v-model="doc.hazards_statements"></textarea></div>
				<label>Precautionary Statements - Prevention</label>
				<div><textarea wrap="off" style="min-height: 300px" v-model="doc.hazards_precaution_statements_prevention"></textarea></div>
				<label>Precautionary Statements - Response</label>
				<div><textarea wrap="off" style="min-height: 300px" v-model="doc.hazards_precaution_statements_response"></textarea></div>
				<label>Precautionary Statements - Storage</label>
				<div><textarea wrap="off" style="min-height: 300px" v-model="doc.hazards_precaution_statements_storage"></textarea></div>
				<label>Precautionary Statements - Disposal</label>
				<div><textarea wrap="off" style="min-height: 300px" v-model="doc.hazards_precaution_statements_disposal"></textarea></div>
				<label>Hazards not otherwise classified (HNOC) - Other Information</label>
				<div><textarea wrap="off" style="min-height: 300px" v-model="doc.hazards_precaution_statements_other"></textarea></div>
			</section>
			<div v-if="loading" class="u1-loading"></div>
		</div>
	</section>
</div>
`,
});
