import { FetchPost, MemberFetchPost } from "./ultra-utils.js";
import ultraMixin from "./ultra-mixin.js";
import ultraEventBus from "./ultra-event-bus.js";

/**
 * How to use:
 * 	//Load and share member's group permissions
		const memberProfile = ref(ultraArchetypeMember.getMemberProfile());
		ultraEventBus.$on('member-profile-loaded', updatedMemberProfile => { 
			memberProfile.value = updatedMemberProfile;
		});
		provide('memberProfile', memberProfile);
 */

class UltraArchetypeMember {
	static memberProfile = false;
	constructor() {
		this.isLoggedIn = false;
	}

	isMemberLoggedIn() {
		return this.isLoggedIn;
	}

	getMemberProfile() {
		//Save the member profile infor to local storage so it can be retrieved
		// immediately without a server call.
		if (this.memberProfile) {
			return this.memberProfile;
		}

		let localMemberProfile = localStorage.getItem('archetype-member-profile');
		if (typeof localMemberProfile == 'undefined' || localMemberProfile == 'undefined') {
			return "";
		}

		localMemberProfile = JSON.parse(localMemberProfile);
		return localMemberProfile;
	}

	setMemberProfile(newMemberProfile) {
		this.memberProfile = newMemberProfile;
		localStorage.setItem('archetype-member-profile', JSON.stringify(newMemberProfile));
		ultraEventBus.$emit('member-profile-loaded', newMemberProfile);
	}

	canAccess(permissions = false, group, action) {
		if (!permissions || !permissions instanceof Object) {
			ultraEventBus.$emit('error', 'Error checking permissions...');
			return false;
		}
		if (!permissions[group]) {
			ultraEventBus.$emit('error', 'Your account\'s is not in the group you are requesting access to.');
			return false;
		}
		if (permissions[group].includes(action)) {
			return true;
		} else {
			ultraEventBus.$emit('error', 'Your account\'s group does not have access this.');
			return false;
		}
	}

	redirectIfNotLoggedIn(router) {
		// Check remote sources for login credentials
		MemberFetchPost(this, "/admin/member-api-json.php?loggedin")
			.then(json => {
				if (json.error || !json.loggedin) {
					this.notLoggedIn(router);
				} else {
					this.isLoggedIn = true;
					this.setMemberProfile(json?.member_profile);
				}
			})
			.catch(error => {
				this.notLoggedIn(router);
			});
	}

	notLoggedIn(router) {
		if (!router) return;
		if (router.currentRoute.name != "login" && router.currentRoute.name != "login-reset")
			router.push({
				name: "login",
				query: { "login-redirect": router.currentRoute.value.fullPath },
			});
	}

	forgotPassword(email, loading, router, route) {
		loading.value = true;
		const formData = {
			email: email.value,
			return_path: window.location.pathname,
		};

		FetchPost("/admin/public-api-json.php?forgotpassword", formData)
			.then(json => {
				loading.value = false;
				if (json.error) {
					ultraMixin.error(json.error);
					ultraEventBus.$emit('forgot-password-email-delivered', { error: json.error });
				} else {
					ultraEventBus.$emit('forgot-password-email-delivered', { success: true });
					ultraMixin.snackbarPersist(json.message);
				}
			})
			.catch(error => {
				loading.value = false;
				console.error(error);
				ultraMixin.error(error);
			});
	}

	validateForgotPasswordKey(key, loading, success, error) {
		loading.value = true;
		const formData = {
			key: key,
		};

		FetchPost("/admin/public-api-json.php?forgotpassword-validate-key", formData)
			.then(json => {
				loading.value = false;
				if (json.error) {
					error(json.error);
				} else {
					success(json);
				}
			})
			.catch(error => {
				loading.value = false;
				console.error(error);
				ultraMixin.error(error);
			});
	}

	resetPassword(key, pw1, pw2, loading, success, error) {
		loading.value = true;
		const formData = {
			key: key,
			password_1: pw1,
			password_2: pw2,
		};

		FetchPost("/admin/public-api-json.php?forgotpassword-reset-password", formData)
			.then(json => {
				loading.value = false;
				if (json.error) {
					error(json.error);
				} else {
					success(json);
				}
			})
			.catch(error => {
				loading.value = false;
				console.error(error);
				ultraMixin.error(error);
			});
	}

	login(email, password, loading, router, route) {
		//Process login...
		loading.value = true;
		const formData = {
			email: email.value,
			password: password.value,
		};

		FetchPost("/admin/member-api-json.php?login", formData)
			.then(json => {
				loading.value = false;
				if (json.error) {
					ultraMixin.error(json.error);
				} else {
					this.isLoggedIn = true;
					this.setMemberProfile(json?.member_profile);

					if (route.currentRoute.value.query["login-redirect"]) {
						router.replace(route.currentRoute.value.query["login-redirect"]);
					} else {
						router.replace({ name: "home" });
					}
				}
			})
			.catch(error => {
				loading.value = false;
				console.error(error);
				ultraMixin.error(error);
			});
	}

	logout(router) {
		//Process logout...
		this.loading = true;
		const formData = {};

		FetchPost("/admin/member-api-json.php?logout", formData)
			.then((json) => {
				this.loading = false;
				if (json.error) {
					this.error(json.error);
				}
				router.replace({ name: "login" });
			})
			.catch((error) => {
				this.loading = false;
				this.error(error);
			});
	}
}

export default new UltraArchetypeMember();
