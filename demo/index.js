import { defineComponent, onMounted, reactive, ref } from '../vue.esm.browser-3.0.js';

export default defineComponent({
	name: "home",
	setup() {
		const loading = ref(true);
		const data = reactive({});

		onMounted(() => { });

		return {
			loading,
			data
		}
	},
	template: `<section class="component-main u1-relative">
	<header class="component-header">Ultra Vue 2 Demo</header>
	<div class="component-body">
		<h2>Tools</h2>
		<nav class="home-nav" style="flex-direction: column; max-width: 320px">
			<router-link to="/form-builder">Form Builder</router-link>
			<router-link to="/table">Table</router-link>
			<router-link to="/auto-table">Auto-Table</router-link>
			<router-link to="/misc">Misc</router-link>
			<router-link to="/db-editor">DB Editor</router-link>
			<router-link to="/should-be-missing">Missing Page</router-link>
			<div class="nav-spacer"></div>
			<router-link class="out" to="/logout" data-cy-test="logout">Logout <i class="u1-i-chevron u1-opacity-50"></i></router-link>
		</nav>
	</div>
</section>
`,
});
