import { defineComponent, defineEmit, onMounted, ref } from "./vue.esm.browser-3.0.js";
/**
 * How to use:
 * <ultra-checkbox-group v-model="item.progress_status"
					:buttons="[{value:'In Progress'},{value:'Next'},{value:'Queue'}]"></ultra-checkbox-group>
 * OR
 * <ultra-checkbox-group v-model="item.progress_status"
					:buttons="[{value:1,label:'In Progress'},{value:2,label:'Next'},{value:3,label:'Queue'}]"></ultra-checkbox-group>
 */
export default defineComponent({
	name: "ultra-checkbox-group",
	props: {
		modelValue: Array,
		buttons: Array,
		htmlLabels: false,
		classSelected: { type: String, default: "u1-checkbox-selected" },
	},
	setup(props) {
		const selected = ref(null);
		function arrayToggleItem(array, item) { return array.includes(item) ? array.filter(i => i !== item) : [...array, item] }

		function updateValue(checkboxValue) {
			selected.value = props.modelValue; // Get current copy of selections from model
			selected.value = arrayToggleItem(selected.value, checkboxValue.toString());
			this.$emit('update:modelValue', selected.value); // previously was `this.$emit('input', title)`
		}

		return {
			props,
			updateValue,
		};
	},
	template: `<section class="u1-checkbox-group">
	<template v-for="(checkboxButton,index) in buttons">
		<label :class="modelValue.includes(checkboxButton.value.toString()) ? classSelected:''">
			<input 
				type="checkbox"
				:value="checkboxButton.value"
				:checked="modelValue.includes(checkboxButton.value.toString())"
				@input="updateValue(checkboxButton.value)" />
			<div v-if="props.htmlLabels" v-html="checkboxButton.label ? checkboxButton.label:checkboxButton.value"
			:class="checkboxButton.class"
			></div>
			<template v-else>{{ checkboxButton.label ? checkboxButton.label:checkboxButton.value }}</template>
		</label>
	</template>
</section>
`,
});
