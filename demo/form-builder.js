import { defineComponent, inject, reactive, ref, resolveDirective } from "../vue.esm.browser-3.0.js";
import UltraMixin from "../ultra-mixin.js";
import UltraEventBus from "../ultra-event-bus.js";
import { MemberFetchPost, FetchPost } from "../ultra-utils.js";
import UltraTable from "../ultra-table.js";
import UltraUploader from "../ultra-uploader.js";

export default defineComponent({
	name: "misc",
	components: { UltraTable, UltraUploader },
	setup() {
		const loading = ref(false);

		const snippets = [
			{
				label: 'Fieldset with button in legend',
				snippet: `<fieldset>
	<legend>
		<div class="u1-flex-100">Categories</div>
		<div>
			<button @click="addCategory()" type="button" class="u1-button u1-button-green">
				<i class="u1-i-plus u1-opacity-50"></i>
				Add Catetgory
			</button>
		</div>
	</legend>
	FIELDSET CONTENT
</fieldset>`,
			},
			{
				label: 'Date with "Now" button',
				snippet: `<label>Date Received</label>
<div class="u1-cols-2-1fr-max">
	<input type="date" v-model="task.date_received" />
	<button @click="setDateReceivedToday()" class="u1-button u1-button-gray" type="button">Today</button>
</div>`},
{
	label: 'Form Structure',
	snippet: `<section class="u1-label-input-rows">
	<div class="u1-checkbox-group" v-show="!task.need_expedited">
		<label>
			<input type="checkbox" v-model="task.need_expedited" :true-value="1" :false-value="0" />
			Need Expedited!
		</label>
	</div>
</section>`},
		];

		const copy = function (text) {
			var dummy = document.createElement("textarea");
			// dummy.style.display = 'none'
			document.body.appendChild(dummy);
			//Be careful if you use texarea. setAttribute('value', value), which works with "input" does not work with "textarea". – Eduard
			dummy.value = text;
			dummy.select();
			document.execCommand("copy");
			document.body.removeChild(dummy);
			UltraEventBus.$emit('ultra-snackbar','Copied!');
		}

		return {
			loading,
			snippets,
			copy,
		}
	},
	template: `<section class="component-main u1-relative">
	<header class="component-header">
		<div class="component-header-title" style="flex-grow: 0"> Misc Components </div>
	</header>
	<div class="component-body" style="--u1-sticky-top: var(--header-height)">
		<section class="u1-form u1-label-input-rows" style="margin-top: 2rem">
			<section v-for="snippet in snippets">
				<label>{{snippet.label}}</label>
				<div
						class="u1-hover u1-pointer"
						@click="copy(snippet.snippet)"
						style="background: #fff; padding: 1rem; border-radius: 0.4rem">
					<pre><code v-text="snippet.snippet" ></code></pre>
				</div>
			</section>
		</section>
	</div>
</section>
`,
});
