import { defineComponent, onMounted, onBeforeUnmount, ref, reactive } from "./vue.esm.browser-3.0.js";
import UltraEventBus from "./ultra-event-bus.js";
import UltraUploaderFile from "./ultra-uploader-file.js";

//DOCS:
//-EVENTS
//--UltraEventBus.$emit("files-selected", uploaderId, filesUploading);
//--UltraEventBus.$emit("file-removed", uploaderId, filesUploading);

/*

WATCH EVENT FROM THIS: 
UltraEventBus.$emit("file-uploaded", uploaderId, file, response);

SNIPPET:
<ultra-uploader accept="*" fullscreenDrop="true" class="userfiles-uploader" multiple="true"
						:uploadDir="uploadDir"
						:uploadUrl="uploadUrl"></ultra-uploader>

SERVER SIDE:
function member_upload_report_image()
	{
		$this->header_json();
		$customer_id = $_GET['customer_id'];

		if(!is_dir($this->dir_report_attached_images_temp))
		{
			$o_filesystem = $this->init_file_system();
			if(!$o_filesystem->create_directory_tree($this->dir_report_attached_images_temp))
			{
				$o_error = $this->init_error();
				$o_error->admin_error_message='Failed to create TEMP_UPLOAD_DIR for INTEGRA reports attaching images.';
				$o_error->submit_error();
				echo json_encode(array('error' => 'Failed to create temporary location for uploaded file.'));
				exit;
			}
		}
		
		if (!is_uploaded_file($_FILES['file']['tmp_name'])) {
			$o_error = $this->init_error();
			$o_error->admin_error_message='Failed t omove_uploaded_file for INTEGRA reports attaching images. $_FILES:'.$path;
			$o_error->submit_error();
			echo json_encode(array('error' => 'There was an error uploading the file, it appears to be invalid.'));
			exit;
		}

		$new_file_name = 'report-image-member-'.$_SESSION['member_id'].'-cust-'.$customer_id.'-t-'.microtime(true);

		$pathinfo = pathinfo($_FILES['file']['name']);
		$filename = $pathinfo['filename'];
		$ext = strtolower($pathinfo['extension']);

		if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
			$these = implode(', ', $this->allowedExtensions);
			echo json_encode(array('error' => 'File has an invalid extension, it should be one of '. $these . '.'));
			exit;
		}

		$path = $this->dir_report_attached_images_temp.$new_file_name.$ext;
		if(!move_uploaded_file($_FILES['file']['tmp_name'], $path)){
			$o_error = $this->init_error();
			$o_error->admin_error_message='Failed to move_uploaded_file() for INTEGRA reports attaching images. $path:'.$path;
			$o_error->submit_error();
			echo json_encode(array('error' => 'There was an error processing the file after upload.'));
			exit;
		}
		$_SESSION['integra_reports'][$customer_id]['attached_images'][] = $new_file_name.$ext;

		$attached_images = $_SESSION['integra_reports'][$customer_id]['attached_images'];
		echo json_encode(array('attached_images'=>$attached_images));
		exit;
	}
*/

export default defineComponent({
	name: "ultra-uploader",
	components: { UltraEventBus, UltraUploaderFile },
	props: {
		uploaderId: { type: String, default: "uploader" },
		fileFieldName: { default: "file" },
		fullscreenDrop: { default: false },
		uploadUrl: String,
		uploadDir: String,
		inputUploadClassDragHover: { default: "u1-upload-dropbox-drag-hover" }, //Required because there is no CSS method to style drag/drop+hover
		accept: { default: "image/jpeg,image/png,image/gif,image/webp,image/svg" },
		multiple: { default: true },
		postData: Array,
	},
	setup(props) {
		let is_uploading = ref(false);
		let is_drag_over = ref(false);
		const filesUploading = reactive([]);
		let inputUploadClass = ref("");

		let {
			uploaderId,
			fileFieldName,
			fullscreenDrop,
			uploadUrl,
			uploadDir,
			inputUploadClassDragHover,
			accept,
			multiple
		} = props;

		let label = `<i class="u1-opacity-50 u1-i-arrow u1-i-rotate-up"></i> <b>Upload:</b> 
		Click or drag your file${multiple === true ? "s" : ""} here to upload...`;

		const files_field = ref(null);
		const form = ref(null);

		const error = function (msg) {
			UltraEventBus.$emit("ultra-snackbar-error", msg);
		}

		const snackbar = function (msg) {
			UltraEventBus.$emit("ultra-snackbar-message", msg);
		}

		const dragDone = function (evt) {
			is_drag_over.value = false;
			evt.preventDefault();
			evt.stopPropagation();
		}

		const drag = function (evt) {
			is_drag_over.value = true;
			evt.preventDefault();
			evt.stopPropagation();
		}

		const dragDrop = function (evt) {
			evt.preventDefault();
			evt.stopPropagation();

			is_drag_over.value = false;
			var dt = evt.dataTransfer;
			uploadFiles(dt.files);
		}

		const onFileInputChange = function (evt) {
			uploadFiles(files_field.value.files);
		}

		const uploadFiles = function (droppedFiles) {
			let files = droppedFiles;
			if (files) {
				// Load previews of images...
				Array.from(files).forEach(file => {
					file.vue_index_key =
						file.name +
						"-" +
						new Date().getSeconds() +
						"-" +
						new Date().getMilliseconds();

					filesUploading.unshift(file);
				});
				UltraEventBus.$emit("uploader-files-selected", uploaderId, filesUploading);
			}
		}

		const removeFile = function (file_to_remove) {
			filesUploading.forEach((file, index) => {
				if (file.vue_index_key == file_to_remove.vue_index_key) {
					filesUploading.splice(index, 1);
				}
			});
			UltraEventBus.$emit("uploader-file-removed", uploaderId, filesUploading);
		}

		const uploadSucceed = (file, response) => {
			removeFile(file);
			UltraEventBus.$emit("file-uploaded", uploaderId, file, response);
		}

		const abortFile = file => {
			removeFile(file);
		}

		onMounted(() => {
			UltraEventBus.$on("upload-success", uploadSucceed);
			UltraEventBus.$on("abort-file", abortFile);

			if (fullscreenDrop) {
				document.body.ondrag = drag;
				document.body.ondragstart = drag;
				document.body.ondragover = drag;
				document.body.ondragenter = drag;
				document.body.ondragend = dragDone;
				document.body.ondragleave = dragDone;
				document.body.ondrop = dragDrop;
			} else {
				form.value.ondrag = drag;
				form.value.ondragstart = drag;
				form.value.ondragover = drag;
				form.value.ondragenter = drag;
				form.value.ondragend = dragDone;
				form.value.ondragleave = dragDone;
				form.value.ondrop = dragDrop;
			}
		});

		onBeforeUnmount(() => {
			UltraEventBus.$off("upload-success", uploadSucceed);
			UltraEventBus.$off("abort-file", abortFile);
		});

		return {
			is_drag_over,
			is_uploading,
			filesUploading,
			inputUploadClass,
			label,
			uploaderId,
			fileFieldName,
			fullscreenDrop,
			inputUploadClassDragHover,
			accept,
			multiple,
			form,
			files_field,
			uploaderId,
			error,
			snackbar,
			onFileInputChange
		}
	},
	template: `
<section class="u1-uploader">
  <form ref="form"
    enctype="multipart/form-data"
		novalidate
		class="u1-upload-form"
  >
    <div :class="(is_drag_over ? inputUploadClassDragHover:'') + ' u1-form u1-upload-dropbox'">
      <input
        type="file"
        :fileFieldName="fileFieldName"
        :disabled="is_uploading"
		:multiple="multiple"
        class="u1-upload-input-file"
        ref="files_field"
        @change.prevent.stop="onFileInputChange($event)"
        :accept="accept"
      />
      <div v-html="label"></div>
		</div>
		<transition name="u1-fade">
			<div v-show="filesUploading.length" class="u1-upload-dropbox-preview">
				<transition-group tag="span" name="u1-fade">
					<ultra-uploader-file
						v-for="file in filesUploading"
						:key="file.vue_index_key"
						:file="file"
						:fileFieldName="fileFieldName"
						:upload-url="uploadUrl"
						:upload-dir="uploadDir"
						:accept="accept"
						:post-data="postData"
						:uploader-id="uploaderId"
					></ultra-uploader-file>
				</transition-group>
			</div>
		</transition>
	</form>
</section>
`,
});
